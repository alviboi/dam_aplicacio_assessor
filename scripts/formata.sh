#!/bin/bash
echo $1
echo $(pwd)
pandoc scripts/temp/out2.md -o $1 --from markdown+implicit_figures --template eisvogel --listings --metadata-file="scripts/conf.yaml" --filter pandoc-latex-environment --toc