#!/bin/bash
echo "Convert docx"
pandoc scripts/temp/input_docx.docx -f docx -t markdown+pipe_tables --markdown-headings=atx --wrap=none --extract-media="scripts/media" -o scripts/temp/out.md