#!/bin/bash
pandoc scripts/temp/out2.md -o scripts/index.html --from markdown+implicit_figures -c scripts/pandoc.css --template elegant_bootstrap_menu --metadata-file="scripts/conf.yaml" --listings --filter pandoc-latex-environment --toc --toc-depth 2 
# --toc-depth 2
