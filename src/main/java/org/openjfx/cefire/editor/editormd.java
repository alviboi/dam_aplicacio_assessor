package org.openjfx.cefire.editor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.openjfx.cefire.exporta_xls.info;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Esqulet per a l'editor bàsic que s'ha creat per afegir alguns detalls si l'usuari ho considera
 * 
 * @author alfredo
 *
 */
public class editormd implements Initializable{
	CreaYaml creayaml;

	@FXML
	private TextArea CampDeText;

	/**
	 * Afegix el codi alert
	 * 
	 * @param event
	 */
	@FXML
	void AfegixALERT(ActionEvent event) {
		int caretPosition = CampDeText.getCaretPosition();
		CampDeText.insertText(caretPosition, ":::alert\n"
				+ "<!-- Afegix el text després d'esta línea -->\n"
				+ ":::\n");
	}

	/**
	 * Obri finestra per a crear el codi en markdown per a afegir una imatge
	 * 
	 * @param event
	 */
	@FXML
	void AfegixIMATGE(ActionEvent event) {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("insertaimatge.fxml"));
		Parent root1 = null;
		try {
			root1 = (Parent) fxmlLoader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Insertaimatge titaut = (Insertaimatge) fxmlLoader.getController();
		titaut.setCampDeText(CampDeText);
		Stage stage = new Stage();
		stage.initModality(Modality.APPLICATION_MODAL);
		Scene scene2 = new Scene(root1);
		stage.setTitle("Inserta imatge");
		stage.getIcons().add(new Image("cefireicon.png"));
		stage.setScene(scene2);
		stage.show();	
	}

	/**
	 * Afegix codi en markdown per a generar un alert
	 * 
	 * @param event
	 */
	@FXML
	void AfegixINFO(ActionEvent event) {
		int caretPosition = CampDeText.getCaretPosition();
		CampDeText.insertText(caretPosition, ":::info\n"
				+ "<!-- Afegix el text després d'esta línea -->\n"
				+ ":::\n");
	}

	/**
	 * Afegix en codi per a generar una marca tip en markdown
	 * 
	 * @param event
	 */
	@FXML
	void AfegixTIP(ActionEvent event) {
		int caretPosition = CampDeText.getCaretPosition();
		CampDeText.insertText(caretPosition, ":::tip\n"
				+ "<!-- Afegix el text després d'esta línea -->\n"
				+ ":::\n");
	}

	/**
	 * Afeix el codi en markdown per a generar un warning
	 * 
	 * @param event
	 */
	@FXML
	void AfegixWARNING(ActionEvent event) {
		int caretPosition = CampDeText.getCaretPosition();
		CampDeText.insertText(caretPosition, ":::caution\n"
				+ "<!-- Afegix el text després d'esta línea -->\n"
				+ ":::\n");
	}

	/**
	 * Genera l'arxiu pdf ja formatat
	 * 
	 * @param event
	 */
	@FXML
	void ArxiuPDF(ActionEvent event) {
		if (creayaml == null) {
			info.infoBox("Cal que confgiures qui és l'autor del text i el títol del mateix", "Avís de format", "Informació", AlertType.INFORMATION);
		} else {
			creayaml.Escriu_arxiu();
			
			FileChooser fileChooser = new FileChooser();      
	        //Set extension filter for text files
	        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("pdf arxius (*.pdf)", "*.pdf");
	        fileChooser.getExtensionFilters().add(extFilter);
	        //Show save file dialog
	        File file = fileChooser.showSaveDialog(CampDeText.getScene().getWindow());       
	        //TODO
	        System.out.println(file.getAbsolutePath());   
	        
	        try {
	        	salva_md();
				String[] commands = {"sh", "-c", "scripts/formata.sh "+file.getAbsolutePath()};
				Runtime r = Runtime.getRuntime();
				Process process = null;
	
				process = r.exec(commands);
				
				try {
					process.waitFor();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				try (var reader = new BufferedReader(
		            new InputStreamReader(process.getErrorStream()))) {

		            String line;

		            while ((line = reader.readLine()) != null) {
		                System.out.println(line);
		            }

		        } catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		        
		        try (var reader = new BufferedReader(
		                new InputStreamReader(process.getInputStream()))) {

		                String line;

		                while ((line = reader.readLine()) != null) {
		                    System.out.println(line);
		                }

		            } catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * Genera l'arxiu zip per a exportat a Aules
	 * 
	 * @param event
	 */
	@FXML
	void ArxiuZIP(ActionEvent event) {
		if (creayaml == null) {
			info.infoBox("Cal que confgiures qui és l'autor del text i el títol del mateix", "Avís de format", "Informació", AlertType.INFORMATION);
		} else {
			creayaml.Escriu_arxiu();
			
			FileChooser fileChooser = new FileChooser();      
	        //Set extension filter for text files
	        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("zip arxius (*.zip)", "*.zip");
	        fileChooser.getExtensionFilters().add(extFilter);
	        //Show save file dialog
	        File file = fileChooser.showSaveDialog(CampDeText.getScene().getWindow());       
	        //TODO
	        System.out.println(file.getAbsolutePath());   
	        
	        try {
	        	salva_md();
				String[] commands = {"sh", "-c", "scripts/crea_html.sh "+file.getAbsolutePath()};
				Runtime r = Runtime.getRuntime();
				Process process = null;
	
				process = r.exec(commands);
				
				try {
					process.waitFor();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				ArrayList <String> Arxius_zip = new ArrayList();
				Arxius_zip.add("scripts/cefire.png");
				Arxius_zip.add("scripts/pandoc.css");
				Arxius_zip.add("scripts/index.html");
				
				// Codid creat per a depurar
				File directoryPath = new File("scripts/media");
			      String contents[] = directoryPath.list();
			      for (String string : contents) {
			    	  Arxius_zip.add("scripts/media/"+string);
			      }
	
				try {
					comprimir(Arxius_zip,file.getAbsolutePath());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				try (var reader = new BufferedReader(
		            new InputStreamReader(process.getErrorStream()))) {

		            String line;

		            while ((line = reader.readLine()) != null) {
		                System.out.println(line);
		            }

		        } catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		        
		        try (var reader = new BufferedReader(
		                new InputStreamReader(process.getInputStream()))) {

		                String line;

		                while ((line = reader.readLine()) != null) {
		                    System.out.println(line);
		                }

		            } catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	/**
	 * Transforma el document docx a markdown
	 * 
	 * @param event
	 */
	@FXML
	void ImportaDOCX(ActionEvent event) {
		FileChooser fileChooser = new FileChooser();
        
        //Set extension filter for text files
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("docx arxius (*.docx)", "*.docx");
        fileChooser.getExtensionFilters().add(extFilter);

        //Show save file dialog
        File file = fileChooser.showOpenDialog(CampDeText.getScene().getWindow());
        
        //TODO
        System.out.println(file.getAbsolutePath());
        
        try {
			convertix_arxiu(file.getAbsolutePath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			info.infoBox(e.toString(), "Error d'excepció", "Error", AlertType.ERROR);
		}
        
	}

	/**
	 * Capta l'event per a enviar l'arxiu a converteix_arxiu
	 * 
	 * @param event
	 */
	@FXML
	void InsertaDocument(DragEvent event) {
		Dragboard db = event.getDragboard();
		boolean success = false;
		try {
			convertix_arxiu(db.getString().substring(6));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			info.infoBox(e1.toString(), "Error d'excepció", "Error", AlertType.ERROR);
		}
	}

	/**
	 * Tanca la finestra
	 * 
	 * @param event
	 */
	@FXML
	void TancaFinestra(ActionEvent event) {
		Stage stage = (Stage) CampDeText.getScene().getWindow();
        stage.close();
	}
	
	/**
	 * Configurar els paràmetres de títol, autor i subtítol
	 * 
	 * @param event
	 */
	@FXML
	void ConfiguraTitolautor(ActionEvent event) {
		creayaml = new CreaYaml();
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("titolautor.fxml"));
		Parent root1 = null;
		try {
			root1 = (Parent) fxmlLoader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Titolautor titaut = (Titolautor) fxmlLoader.getController();
		titaut.setArxiuyaml(creayaml);
		Stage stage = new Stage();
		stage.initModality(Modality.APPLICATION_MODAL);
		Scene scene2 = new Scene(root1);
		stage.setTitle("Configura títol i autor");
		stage.getIcons().add(new Image("cefireicon.png"));
		stage.setScene(scene2);
		stage.show();	    
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		CampDeText.setOnDragOver(event -> {
			event.acceptTransferModes(TransferMode.MOVE);
		});			
	}
	
	/**
	 * Convertix l'arxiu
	 * 
	 * @param file_db
	 * @throws IOException
	 */
	public void convertix_arxiu(String file_db) throws IOException {
			
			System.out.println(file_db);
			System.out.println(file_db.substring(0,5));
			System.out.println(file_db.substring(6));
			System.out.println(file_db.substring(file_db.length()-4,file_db.length()));
			String extensio = file_db.substring(file_db.length()-4,file_db.length());
			String arxiu = file_db;
			if (extensio.equals("docx")) {
				Path sourceDirectory = Paths.get(arxiu);
				Path targetDirectory = Paths.get("./scripts/temp/input_docx.docx");
				Path outfile= Paths.get("scripts/temp/out.md");
				Path outmedia= Paths.get("./scripts/temp/media/");
				Files.deleteIfExists(targetDirectory);

				try {
					Files.copy(sourceDirectory, targetDirectory);
					String[] commands = {"sh", "-c", "scripts/convertdocx.sh"};
					Runtime r = Runtime.getRuntime();
					Process process = null;

					process = r.exec(commands);
					
					try {
						process.waitFor();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					try  
					{  
						List<String> lines = Collections.emptyList();  
						lines=Files.readAllLines(Paths.get("scripts/temp/out.md"), StandardCharsets.UTF_8);
						for (String string : lines) {
							CampDeText.appendText(string+"\n");
						}		
					}  
					catch(Exception e)  
					{  
						e.printStackTrace();  
					}  			
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			info.infoBox("L'arxiu "+file_db+ " no es pot convertir", "Avís de format", "Error", AlertType.WARNING);
		}
	}
	
	/**
	 * Salva el markdown a un arxiu que s'utilitzarà per a formatar després el document
	 * 
	 * @throws IOException
	 */
	public void salva_md () throws IOException{
		Files.deleteIfExists(Paths.get("scripts/temp/out2.md"));
		try {
            FileWriter writer = new FileWriter("scripts/temp/out2.md", true);
            writer.write(CampDeText.getText());
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
	}
	
	/**
	 * Crea l'arxiu per a comprimir
	 * 
	 * @param Arxius_zip
	 * @param outputFile
	 * @throws Exception
	 */
	public void comprimir(ArrayList <String> Arxius_zip, String outputFile) throws Exception {
		Path ou = Paths.get(outputFile);
		try (ZipOutputStream zipOutputStream = new ZipOutputStream(Files.newOutputStream(ou))) {
            for (String file : Arxius_zip) {
            	ZipEntry zipEntry = null;
            	if (file.equals("scripts/index.html")) {
            		zipEntry = new ZipEntry(file.substring(8));
            	} else {
            		zipEntry = new ZipEntry(file);
            	}
                
                zipOutputStream.putNextEntry(zipEntry);
                Path in = Paths.get(file);
                Files.copy(in, zipOutputStream);
            }
		}
	}
}
