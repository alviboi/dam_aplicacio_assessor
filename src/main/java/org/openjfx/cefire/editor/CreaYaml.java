package org.openjfx.cefire.editor;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;

/**
 * Aquesta classe crea un arxiu YAML per poder generar els documents
 * 
 * @author alfredo
 *
 */
public class CreaYaml {
	String title;
	String author;
	String subtitle;
	public CreaYaml(String title, String author, String subtitle) {
		super();
		this.title = title;
		this.author = author;
		this.subtitle = subtitle;
	}
	public CreaYaml() {
		// TODO Auto-generated constructor stub
		super();
		this.title = "";
		this.author = "";
		this.subtitle = "";
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getSubtitle() {
		return subtitle;
	}
	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}	
	
	/**
	 * 
	 * Aquest és el mètode que genera el document
	 * 
	 */
	public void Escriu_arxiu() {
		LocalDate a = LocalDate.now();
	    try {
	      FileWriter yaml = new FileWriter("scripts/conf.yaml");
	      yaml.write("---\n"
	      		+ "title: \""+this.getTitle()+"\"\n"
	      		+ "author: ["+this.getAuthor()+"]\n"
	      		+ "date: \""+a.toString()+"\"\n"
	      		+ "subject: \"Cefire\"\n"
	      		+ "keywords: [cefire, curs]\n"
	      		+ "subtitle: \""+this.subtitle+"\"\n"
	      		+ "lang: \"es\"\n"
	      		+ "page-background: \"scripts/background10.pdf\"\n"
	      		+ "titlepage: true,\n"
	      		+ "titlepage-rule-color: \"360049\"\n"
	      		+ "titlepage-background: \"scripts/background10.pdf\"\n"
	      		+ "colorlinks: true\n"
	      		+ "toc-own-page: true\n"
	      		+ "header-includes:\n"
	      		+ "- |\n"
	      		+ "  ```{=latex}\n"
	      		+ "  \\usepackage{awesomebox}\n"
	      		+ "  \\usepackage{caption}\n"
	      		+ "  \\usepackage{array}\n"
	      		+ "  \\usepackage{tabularx}\n"
	      		+ "  \\usepackage{ragged2e}\n"
	      		+ "  \\usepackage{multirow}\n"
	      		+ "\n"
	      		+ "\n"
	      		+ "  ```\n"
	      		+ "pandoc-latex-environment:\n"
	      		+ "  noteblock: [info]\n"
	      		+ "  tipblock: [tip]\n"
	      		+ "  warningblock: [warning]\n"
	      		+ "  cautionblock: [caution]\n"
	      		+ "  importantblock: [important]\n"
	      		+ "...");
	      yaml.close();
	      System.out.println("Generat correctament");
	    } catch (IOException e) {
	      System.out.println("Error");
	      e.printStackTrace();
	    }
		  
		}
}
