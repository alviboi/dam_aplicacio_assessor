package org.openjfx.cefire.editor;

import java.net.URL;
import java.util.ResourceBundle;

import org.openjfx.cefire.exporta_xls.info;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;

/**
 * Finestra que agafa les dades necessàries per a generar l'arxiu yaml
 * 
 * @author alfredo
 *
 */
public class Titolautor implements Initializable {
	
	CreaYaml arxiuyaml;

    public CreaYaml getArxiuyaml() {
		return arxiuyaml;
	}

	public void setArxiuyaml(CreaYaml arxiuyaml) {
		this.arxiuyaml = arxiuyaml;
	}

	@FXML
    private TextField autor;

    @FXML
    private TextField subtitol;

    @FXML
    private TextField titol;

    @FXML
    void Guarda_dades(ActionEvent event) {
    	arxiuyaml.setAuthor(autor.getText());
    	arxiuyaml.setSubtitle(subtitol.getText());
    	arxiuyaml.setTitle(titol.getText());
    	info.infoBox("Dades configurades correctament", "Dades configurades", "Informació", AlertType.INFORMATION);
    }

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}

}