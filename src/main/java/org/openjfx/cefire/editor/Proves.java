package org.openjfx.cefire.editor;


import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;


import javafx.stage.Stage;
/**
 * Aqeusta classe s'utiltiza per a provar codi abans de ser implementat
 * 
 * @author alfredo
 *
 */
public class Proves {

	public static void main(String[] args) {
		//Creating a File object for directory
	      File directoryPath = new File("scripts/media");
	      //List of all files and directories
	      String contents[] = directoryPath.list();
	      for(int i=0; i<contents.length; i++) {
	         System.out.println(contents[i]);
	      }
	}
	
	public static void arranca() {

		
		// TODO Auto-generated method stub
		Runtime r = Runtime.getRuntime();
		//String[] commands = {"sh", "-c", "scripts/prova.sh"};
		String[] commands = {"sh", "-c", "scripts/convertdocx.sh"};
		Process process = null;
		try {
			process = r.exec(commands);
			
			InputStream processStdOutput = process.getInputStream();
			
			InputStreamReader re = new InputStreamReader(processStdOutput);
		    BufferedReader br = new BufferedReader(re);
		    String line;
		    while ((line = br.readLine()) != null) {
		      System.out.println(line); // the output is here
	
		    }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			process.waitFor();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}



