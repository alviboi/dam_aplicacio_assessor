package org.openjfx.cefire.editor;

import java.io.File;
import java.io.IOException;

import org.openjfx.cefire.exporta_xls.info;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * Esquelet de la classe per a la finestra inserta una imatge en markdown
 * 
 * @author alfredo
 *
 */
public class Insertaimatge {

    @FXML
    private TextField imatge;

    @FXML
    private TextField peu;

    @FXML
    private TextField width;
    
    private TextArea CampDeText;

	public void setCampDeText(TextArea campDeText) {
		CampDeText = campDeText;
	}

	/**
	 * Afegix el codi en markdown
	 * 
	 * @param event
	 */
	@FXML
    void Afegix_codi(ActionEvent event) {
		int caretPosition = CampDeText.getCaretPosition();
		CampDeText.appendText("!["+this.peu.getText()+"]("+this.imatge.getText()+"){ width="+this.width.getText()+"px }\n"
				+ "");
    	Stage stage = (Stage) width.getScene().getWindow();
        stage.close();
    }

    /**
     * Busca la imatge que vols insertar
     * 
     * @param event
     */
    @FXML
    void busca_imatge(ActionEvent event) {
    	FileChooser fileChooser = new FileChooser();      
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("png arxius (*.png)", "*.png", "*.jpg");
        fileChooser.getExtensionFilters().add(extFilter);
        File file = fileChooser.showOpenDialog(imatge.getScene().getWindow());       
        System.out.println(file.getAbsolutePath());   
        imatge.setText(file.getAbsolutePath());
    }

}
