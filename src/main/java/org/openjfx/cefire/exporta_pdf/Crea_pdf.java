package org.openjfx.cefire.exporta_pdf;

import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import be.quodlibet.boxable.*;
//import be.quodlibet.boxable.line.LineStyle;
import data.CefireDAO;
import data.CompensaDAO;
import data.CursDAO;
import data.EixidaDAO;
import data.GuardiaDAO;
import data.PermisDAO;
import data.UsersDAO;
import domain.Cefire;
import domain.Compensa;
import domain.Curs;
import domain.Guardia;
import domain.Permis;
import domain.Users;
import javafx.scene.text.Text;
import javafx.scene.control.ProgressIndicator;

/**
 * Aquesta classe agafa les dades d'una data determinada per a imprimir els informes del mes
 * 
 * @author alfredo
 *
 */
public class Crea_pdf implements Runnable{
	
	int any;
	int mes; 
	String outputFileName; 
	ProgressIndicator progresarxiu;
	Text arxiu_txt;

    @SuppressWarnings("exports")
	public Crea_pdf(int any, int mes, String outputFileName, ProgressIndicator progresarxiu, Text arxiu_txt) {
		super();
		this.any = any;
		this.mes = mes;
		this.outputFileName = outputFileName;
		this.progresarxiu = progresarxiu;
		this.arxiu_txt = arxiu_txt;
	}

	static List<?> cefire;
    static List<?> compensa;
    static List<?> curs;
    static List<?> guardia;
    static List<?> permis;
    static List<?> eixides;
    static List<?> usuaris;
    static int dia=1;
    

    //ProgressBar Progress_bar
    /**
     * Métode que agafa l'arxiu i l'imprimix
     * 
     * @param any
     * @param mes
     * @param outputFileName
     * @param progresarxiu
     * @param arxiu_txt
     * @throws Exception
     */
    @SuppressWarnings("exports")
	public static void Agafa_arxiu (int any, int mes, String outputFileName, ProgressIndicator progresarxiu, Text arxiu_txt ) throws Exception {
    	LocalDate hui = LocalDate.now();
    	LocalDate data_agafant = LocalDate.of(any+1900, mes+1, 1);
    	//String outputFileName = "SimpleTable2.pdf";
    	//outputFileName = "SimpleTable2.pdf";
    	CefireDAO cefdao = new CefireDAO();
        
        UsersDAO usersdao = new UsersDAO();
        usuaris = usersdao.listusers();
        
        PDDocument document = new PDDocument();
        
        double progres = 0;
        progresarxiu.setProgress(0.1);
		for (Object o : usuaris) {
			progres++;
			
			progresarxiu.setProgress(progres/usuaris.size());
			dia = 1;
			Users o2 = (Users) o;
			System.out.println(o2.getName());
			cefire = cefdao.listCefire_mes_user(any, mes, o2.getId());
	        CompensaDAO compdao = new CompensaDAO();
	        compensa = compdao.listCompensa_mes_user(any, mes, o2.getId());
	        CursDAO cursdao = new CursDAO();
	        curs = cursdao.listCurs_mes_user(any, mes, o2.getId());
	        GuardiaDAO guardiadao = new GuardiaDAO();
	        guardia = guardiadao.listGuardia_mes_user(any, mes, o2.getId());
	        PermisDAO permisdao = new PermisDAO();
	        permis = permisdao.listPermis_mes_user(any, mes, o2.getId());
	        EixidaDAO eixidadao = new EixidaDAO();
	        eixides = eixidadao.listEixides_mes_user(any, mes, o2.getId());
        
        
        

        PDPage page = new PDPage(PDRectangle.A4);
        //PDRectangle rect = page.getMediaBox();
        document.addPage(page);
        
        PDPageContentStream cos = new PDPageContentStream(document, page);

        
        //Afegir Logo CEFIRE
        PDImageXObject imageXObject = PDImageXObject.createFromFile("/home/alfredo/Escritorio/índice.png", document);


        cos.drawImage(imageXObject, 330, 780, imageXObject.getWidth()/2,imageXObject.getHeight()/2);
        
        
        //Títol de informe
        cos.beginText();

        cos.setFont(PDType1Font.TIMES_ROMAN, 12);
        cos.setLeading(14.5f);

        cos.newLineAtOffset(160, 730);
        String line1 = "INFORME DE FITXATGE DEL CEFIRE DE VALÈNCIA";
        cos.showText(line1);

        //cos.newLine();
        cos.newLineAtOffset(50, -350);
        String line2 = "A València a, " + hui.getDayOfMonth() + " " + hui.getMonth().getDisplayName(TextStyle.FULL , Locale.forLanguageTag("ca-ES")) + " de " + hui.getYear();

        cos.showText(line2);
        
        
        float margin = 50;
        float yStartNewPage = page.getMediaBox().getHeight() - (2 * margin);
        float tableWidth = page.getMediaBox().getWidth() - (2 * margin);

        boolean drawContent = true;
        //float yStart = yStartNewPage;
        float bottomMargin = 70;
        // y position is your coordinate of top left corner of the table
        float yPosition = 700;

        BaseTable table = new BaseTable(yPosition, yStartNewPage, bottomMargin, tableWidth, margin, document, page, true, drawContent);
        
        Row<PDPage> row = table.createRow(20);
        Cell<PDPage> cell = row.createCell(90, "ASSESSOR/A: " + o2.getName());
        cell.setRightBorderStyle(null);
        cell.setBottomBorderStyle(null);
        cell = row.createCell(15, "");
        cell.setLeftBorderStyle(null);
        cell.setBottomBorderStyle(null);
        row = table.createRow(20);
        
        cell = row.createCell(90, "<b>Mes " + data_agafant.getMonth().getDisplayName(TextStyle.FULL , Locale.forLanguageTag("ca-ES")) + " de l'any " + data_agafant.getYear() + "</b>");
        cell.setTopBorderStyle(null);
        cell.setRightBorderStyle(null);
        cell = row.createCell(15, "");
        cell.setTopBorderStyle(null);
        cell.setLeftBorderStyle(null);
        row = table.createRow(30);

        int a = dies_de_la_primera_setmana(4,2022)-1;
        if (a == 0) {
        	a = 7;
        }
        
        //System.out.println(Integer.toString(a));
        for (int j = 1; j < 8; j++) {
    		if (j>=(a)) {
    			String celda_str = "<b>Dia: "+Integer.toString(dia) + "</b><br>";
    			celda_str = emplena(celda_str);
    			cell = row.createCell(15, celda_str);
    			if (celda_str != null) {
    				celda_str = celda_str.replace("\n", "").replace("\r", "");
    			} 
                cell.setFontSize(4);
                dia++;
    		} else {
    			cell = row.createCell(15,"");
    		}
		}
        //System.out.println(dia);
        int ultim_dia = getLastDateOfMonth(4,2022);
        int setmanes = (int)(((ultim_dia-(dia-1))/7));
        System.out.println(Integer.toString(setmanes+1));
        setmanes = setmanes + 1;
        for (int i = 0; i < setmanes; i++) {        	
        	row = table.createRow(10);
        	for (int j = 1; j < 8; j++) {
        		if (dia < ultim_dia) {
        			String celda_str = "<b>Dia: "+Integer.toString(dia) + "</b><br>";
        			celda_str = emplena(celda_str); 
        			if (celda_str != null) {
        				celda_str = celda_str.replace("\n", "").replace("\r", "");
        			}        			
        			cell = row.createCell(15, celda_str);
        			cell.setLineSpacing(2);
                    cell.setFontSize(4);
                    dia++;
        		} else {
        			
        		}
        	}
    	}
			
    
        table.draw();
        cos.close();
		}
		
        document.save(outputFileName);
        document.close();
        
        //arxiu_txt.setText("S'ha acabat de crear l'informe");
        
    }
    
    @SuppressWarnings("deprecation")
	private static String emplena(String celda_str) {
		// TODO Auto-generated method stub
    	for (Object object : cefire) {
			Cefire o = (Cefire) object;
			if (o.getData().getDate() == dia) {
				celda_str += o.getInici().toString().substring(0,5) + "-" + o.getFi().toString().substring(0,5) + " CEFIRE<br>";
				//cefire.remove(o);
			}		 
		}
		for (Object object : compensa) {
			Compensa o = (Compensa) object;
			if (o.getData().getDate() == dia) {
				celda_str += o.getInici().toString().substring(0,5) + "-" + o.getFi().toString().substring(0,5) + " COMPENSA: " + o.getMotiu() + "<br>";
				//compensa.remove(o);
			}		 
		}
		for (Object object : curs) {
			Curs o = (Curs) object;
			if (o.getData().getDate() == dia) {
				celda_str += o.getInici().toString().substring(0,5) + "-" + o.getFi().toString().substring(0,5) + " CURS: " + o.getCurs() + "<br>";
				//curs.remove(o);
			}		 
		}
		for (Object object : guardia) {
			Guardia o = (Guardia) object;
			if (o.getData().getDate() == dia) {
				celda_str += o.getInici().toString().substring(0,5) + "-" + o.getFi().toString().substring(0,5) + " GUARDIA<br>";
				//cefire.remove(o);
			}		 
		}
		for (Object object : permis) {
			Permis o = (Permis) object;
			if (o.getData().getDate() == dia) {
				celda_str += o.getInici().toString().substring(0,5) + "-" + o.getFi().toString().substring(0,5) + " PERMIS: " + o.getMotiu() + "<br>";
				//curs.remove(o);
			}		 
		}
		
		return celda_str;
	}

	public static int getLastDateOfMonth(int month, int year){
    	Calendar cal = Calendar.getInstance();
    	cal.set(Calendar.MONTH, month);
    	cal.set(Calendar.YEAR, year);
    	cal.set(Calendar.DAY_OF_WEEK, 1);
        return cal.getActualMaximum(Calendar.DAY_OF_MONTH);
    }
    
    public static int dies_de_la_primera_setmana(int month, int year){
    	Calendar cal = Calendar.getInstance();
    	cal.set(Calendar.MONTH, month);
    	cal.set(Calendar.YEAR, year);
    	cal.set(Calendar.DAY_OF_MONTH, 1);
    	System.out.println(cal.getTime().toString());
    	return cal.get(Calendar.DAY_OF_WEEK);
    }

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			Agafa_arxiu (any, mes, outputFileName, progresarxiu, arxiu_txt );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}