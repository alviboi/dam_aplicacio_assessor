/**
 * 
 */
package org.openjfx.cefire.exporta_pdf;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 * Aquesta classe ens permet cridar a la finestra per a donar les notificacions personalitzades
 * 
 * @author alviboi
 *
 */
public class info {
	@SuppressWarnings("exports")
	public static Alert infoBox2(String infoMessage, String headerText, String title, AlertType a) {
        Alert alert = new Alert(a);
        // AlertType.WARNING INFORMATION INFORMATION CONFIRMATION
        alert.setAlertType(a);
        alert.setContentText(infoMessage);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.show();
        return alert;

    }
	
	@SuppressWarnings("exports")
	public static void infoBox2tanca (Alert a) {
		a.close();
    }
	
	@SuppressWarnings("exports")
	public static void infoBox(String infoMessage, String headerText, String title, AlertType a) {
        Alert alert = new Alert(a);
        // AlertType.WARNING INFORMATION INFORMATION CONFIRMATION
        alert.setAlertType(a);
        alert.setContentText(infoMessage);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.showAndWait();

    }
}
