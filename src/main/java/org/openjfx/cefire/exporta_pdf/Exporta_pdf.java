package org.openjfx.cefire.exporta_pdf;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;

/**
 * Aquesta classe es comónd e l'esquelet de la finestra per a exportar l'informe de fitxatges.
 * 
 * @author alfredo
 *
 */
public class Exporta_pdf implements Initializable{

    @FXML
    private Text arxiu_txt;

    @FXML
    private HBox indicador;

    @FXML
    private ProgressIndicator progresarxiu;

    @FXML
    private DatePicker data_Escollida;

    @FXML
    void Exporta_informes (ActionEvent event) throws Exception {
    	System.out.println(Integer.toString(data_Escollida.getValue().getYear()));
    	System.out.println(Integer.toString(data_Escollida.getValue().getMonthValue()));
    	indicador.setVisible(true);
    	
    	FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PDF arxius (*.pdf)", "*.pdf");
        fileChooser.getExtensionFilters().add(extFilter);
        File file = fileChooser.showSaveDialog(data_Escollida.getScene().getWindow());
        
        if (file != null) {
        	new Thread(new Crea_pdf(data_Escollida.getValue().getYear()-1900, data_Escollida.getValue().getMonthValue()-1, file.getAbsolutePath(), progresarxiu, arxiu_txt)).start();
			//Crea_pdf.Agafa_arxiu(data_Escollida.getValue().getYear()-1900, data_Escollida.getValue().getMonthValue()-1, file.getAbsolutePath(), progresarxiu);					
        }  
        
    }

	@Override
	public void initialize(URL location, ResourceBundle resources) {

	}

}
