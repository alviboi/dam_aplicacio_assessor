package org.openjfx.cefire;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

import org.openjfx.cefire.exporta_xls.info;

import data.VisitaDAO;
import domain.Visita;
import javafx.event.ActionEvent;

/**
 * Esquelñet per a 'enquesta.fxml'. Aquesta finestra mostra tota la informació d'una entrevista seleccionada
 */

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class Enquesta implements Initializable{
	
	Visita visita;

	@FXML
    private TextField FII_desenvolupament;

    @FXML
    private TextField FII_dificultats;

    @FXML
    private TextField FI_Primeres;

    @FXML
    private TextField FI_dificultats;

    @FXML
    private CheckBox PIIE;

    @FXML
    private TextField altres_projectes;

    @FXML
    private TextField altres_questions_interes;

    @FXML
    private TextField definitius;

    @FXML
    private CheckBox erasmus;

    @FXML
    private CheckBox imoute;

    @FXML
    private TextField impacte_formacio;

    @FXML
    private TextField line_ped_pec;

    @FXML
    private HBox linea_pedagogica_hbox;

    @FXML
    private TextField participacio_acc_for;

    @FXML
    private TextField practiques;

    @FXML
    private TextField projectes_destacar;

    @FXML
    private TextField total_plantilla;
    
    @FXML
    private Text nom_centre;
    
    @FXML
    private Text codi_centre;
    
    /**
     * Crida a la classe per a crear el informe
     * 
     * @param event
     */
    @FXML
    void baixa_pdf(ActionEvent event) {

  	
    	FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PDF arxius (*.pdf)", "*.pdf");
        fileChooser.getExtensionFilters().add(extFilter);

        File file = fileChooser.showSaveDialog(codi_centre.getScene().getWindow());
        
        if (file != null) {
        	new Thread(new Crea_pdf(visita, file.getAbsolutePath())).start();
        }
    }
    
    /**
     * Tanca la finestra
     * 
     * @param event
     */
    @FXML
    void tanca_finestra(ActionEvent event) {
    	Stage stage = (Stage) codi_centre.getScene().getWindow();
        stage.close(); 
    }
    
    /**
     * Guarda la visita
     * 
     * @param event
     */
    @FXML
    void salva_visita(MouseEvent event) {
    	VisitaDAO visitadao = new VisitaDAO();
    	Visita visita_salva = this.visita;
    	
    	visita_salva.setFIIDesenvolupament(FII_desenvolupament.getText());	    
    	visita_salva.setFIIDificultatsTroben(FII_dificultats.getText());	    
    	visita_salva.setFIPrimeresImpr(FI_Primeres.getText());	    
    	visita_salva.setFIDificultatsTrobades(FI_dificultats.getText());	    
    	visita_salva.setPIIE(PIIE.isSelected()? 1 : 0);	    
    	visita_salva.setAltresProjectes(altres_projectes.getText());	    
    	visita_salva.setEspais(altres_questions_interes.getText());	    
    	visita_salva.setDefinitius(Integer.parseInt(definitius.getText()));	    
    	visita_salva.setErasmus(erasmus.isSelected()? 1 : 0);	    
    	visita_salva.setiMoute(imoute.isSelected()? 1 : 0);	    
    	visita_salva.setImpacte(impacte_formacio.getText());	    
    	visita_salva.setLiniaPedagogica(line_ped_pec.getText()); 	    
    	visita_salva.setParticipacioAF(participacio_acc_for.getText());	    
    	visita_salva.setFuncPract(Integer.parseInt(practiques.getText()));	    
    	visita_salva.setProjectesDestacables(projectes_destacar.getText());	    
    	visita_salva.setTotal(Integer.parseInt(total_plantilla.getText()));	     
    	visitadao.save_visita(visita_salva);
    	
    	info.infoBox("Dades enviades a la base de dades per a ser guardades", "Informació de les dades", "Avís", AlertType.INFORMATION);
    }
    

    /**
     * Per aquells assessors que escriuen molt poden expandir el camp per a escriure tot el que dessitgen
     * 
     * @param event
     */
    @FXML
    void expandeix_camp(ActionEvent event) {
    	Button a = (Button) event.getSource();
    	HBox b = (HBox) a.getParent();
    	TextField tv = (TextField) b.getChildren().get(0);
    	
    	//Crea una nova finestra        
        
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("enquesta_edita.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            Enquesta_edita homeController = (Enquesta_edita) fxmlLoader.getController();
            
            //ArrayList <Centres> centres;
            //CentresDAO centresdao = new CentresDAO();
            //centres = centresdao.listcentres();
            homeController.set_text(tv);
            
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            Scene scene2 = new Scene(root1);
            stage.setScene(scene2);

            stage.show();
        } catch(Exception e) {
            e.printStackTrace();
        }


    }
    
    /**
     * Mostra advertències
     * 
     * @param a
     */
    public void alert (String a) {

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Alert");
        ButtonType type = new ButtonType("Ok", ButtonData.OK_DONE);
        alert.setContentText(a);
        alert.getDialogPane().getButtonTypes().add(type);
        alert.showAndWait();
    }

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
	}
	
	/**
	 * Emplenes els camps partitn de la classe visita
	 * 
	 * @param visita
	 */
	public void emplenaCamps (Visita visita) {
		
		this.visita = visita;
		
	     FII_desenvolupament.setText(visita.getFIIDesenvolupament());	    
	     FII_dificultats.setText(visita.getFIIDificultatsTroben());	    
	     FI_Primeres.setText(visita.getFIPrimeresImpr());	    
	     FI_dificultats.setText(visita.getFIDificultatsTrobades());	    
	     PIIE.setSelected((visita.getPIIE() != 0) ? true : false);	    
	     altres_projectes.setText(visita.getAltresProjectes());	    
	     altres_questions_interes.setText(visita.getEspais());	    
	     definitius.setText(String.valueOf(visita.getDefinitius()));	    
	     erasmus.setSelected((visita.getErasmus() != 0) ? true : false);	    
	     imoute.setSelected((visita.getiMoute() != 0) ? true : false);	    
	     impacte_formacio.setText(visita.getImpacte());	    
	     line_ped_pec.setText(visita.getLiniaPedagogica());	    
	     participacio_acc_for.setText(visita.getParticipacioAF());	    
	     practiques.setText(String.valueOf(visita.getFuncPract()));	    
	     projectes_destacar.setText(visita.getProjectesDestacables());	    
	     total_plantilla.setText(String.valueOf(visita.getTotal()));	     
	     nom_centre.setText(visita.getNom());	     
	     codi_centre.setText(String.valueOf(visita.getCodi()));

	}

}

