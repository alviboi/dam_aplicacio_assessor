package org.openjfx.cefire;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * Aquesta classe crea l'esquelet de pantalla principal des d'on s'obrin els diferents mòduls
 * 
 * @author alfredo
 *
 */
public class PrimaryController {

	
    /**
     * Mostra la finestra per a poder enviar les dades del control d'assistència
     * 
     * @param event
     */
    @FXML
    void Assigna(MouseEvent event) {
    	FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("exporta_xls/exporta.fxml"));
        Parent root1 = null;
		try {
			root1 = (Parent) fxmlLoader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        Stage stage = new Stage();
        stage.setTitle("Control d'assitència");
        stage.getIcons().add(new Image("cefireicon.png"));
        Scene scene2 = new Scene(root1);
        stage.setScene(scene2);

        stage.show();
    
    }

    /**
     * Mostra la finestra de les entrevistes
     * 
     * @param event
     * @throws IOException
     */
    @FXML
    void Enquesta(MouseEvent event) throws IOException {

	        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("tabla_visites.fxml"));
            Parent root1 = null;
			try {
				root1 = (Parent) fxmlLoader.load();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

            Stage stage = new Stage();
            //stage.initModality(Modality.APPLICATION_MODAL);
            stage.setTitle("Visites a centre");
            stage.getIcons().add(new Image("cefireicon.png"));
            Scene scene2 = new Scene(root1);
            stage.setScene(scene2);

            stage.show();
	    
    }

    /**
     * Obre el mòdul per a exportar l'informe de fitxatges
     * 
     * @param event
     */
    @FXML
    void ExportaPDF(MouseEvent event) {
    	FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("exporta_pdf.fxml"));
        Parent root1 = null;
		try {
			root1 = (Parent) fxmlLoader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        Stage stage = new Stage();
        stage.setTitle("Exporta informe d'assistència");
        stage.getIcons().add(new Image("cefireicon.png"));
        Scene scene2 = new Scene(root1);
        stage.setScene(scene2);
        stage.show();
    
    }

    /**
     * Obre el mòdul per a maquetar els documents
     * 
     * @param event
     */
    @FXML
    void Maqueta(MouseEvent event) {
    	FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("editor/editormd.fxml"));
        Parent root1 = null;
		try {
			root1 = (Parent) fxmlLoader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
      
        //Enquesta enquesta = (Enquesta) fxmlLoader.getController();
        Stage stage = new Stage();
        stage.setTitle("Maquetació de documents");
        stage.getIcons().add(new Image("cefireicon.png"));
        //stage.initModality(Modality.APPLICATION_MODAL);
        Scene scene2 = new Scene(root1);
        scene2.getRoot().requestFocus();
        stage.setScene(scene2);
        stage.show();
    
    }  

}
