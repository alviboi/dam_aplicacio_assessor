package org.openjfx.cefire;

import java.text.MessageFormat;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import be.quodlibet.boxable.*;
import domain.Visita;


public class Crea_pdf implements Runnable{
	
	Visita visita;
	String outputFileName;
	
	
    /**
     * Crea la classe
     * 
     * @param visita
     * @param outputFileName
     */
    public Crea_pdf(Visita visita, String outputFileName) {
		super();
		this.visita = visita;
		this.outputFileName = outputFileName;
	}

    
    /**
     * Aquest métode crea l'arxiu pdf de l'informe de les entrevistes fetes alas centres
     * 
     * @param outputFileName
     * @throws Exception
     */
    @SuppressWarnings("deprecation")
	public void Agafa_arxiu (String outputFileName ) throws Exception {
        
        PDDocument document = new PDDocument();
        
        PDPage page = new PDPage(PDRectangle.A4);
        document.addPage(page);
        
        PDPageContentStream cos = new PDPageContentStream(document, page);
 
        //Afegir Logo CEFIRE
        PDImageXObject imageXObject = PDImageXObject.createFromFile("/home/alfredo/Escritorio/índice.png", document);

        cos.drawImage(imageXObject, 330, 770, imageXObject.getWidth()/2,imageXObject.getHeight()/2);

        //Títol de informe
        cos.beginText();

        float margin = 50;
        float yStartNewPage = page.getMediaBox().getHeight() - (2 * margin);
        float tableWidth = page.getMediaBox().getWidth() - (2 * margin);

        boolean drawContent = true;
        float bottomMargin = 70;
        float yPosition = 700;

        BaseTable table = new BaseTable(yPosition, yStartNewPage, bottomMargin, tableWidth, margin, document, page, true, drawContent);
        //90 70 35
        Row<PDPage> row = table.createRow(60);
        
        row.setLineSpacing(1f);
        Cell<PDPage> cell = row.createCell(99, "<b>INFORME VISITA CENTRES</b> ",HorizontalAlignment.get("center"), VerticalAlignment.get("top"));
        cell.setRightBorderStyle(null);
        cell.setLeftBorderStyle(null);
        cell.setTopBorderStyle(null);
        cell.setFontSize(18);
        
        cell = row.createCell(6, "");
        cell.setLeftBorderStyle(null);
        cell.setTopBorderStyle(null);
        cell.setRightBorderStyle(null);
        //cell.setBottomBorderStyle(null);
       
        
        
        row = table.createRow(20);
        cell = row.createCell(70, "<b>CENTRE:</b> " + visita.getNom());
        cell.setFontSize(12);
        cell.setRightBorderStyle(null);
        //cell.setBottomBorderStyle(null);        
        cell = row.createCell(35, "<b>CODI:</b> " + visita.getCodi());
        cell.setLeftBorderStyle(null);
        //cell.setBottomBorderStyle(null);
        cell.setFontSize(12);
        
        
        row = table.createRow(30);
        cell = row.createCell(90, MessageFormat.format("<b>Dia de la visita: </b>{0}", visita.getData().toLocaleString()));
        cell.setRightBorderStyle(null);
        //cell.setBottomBorderStyle(null); 
        cell = row.createCell(15, "");
        cell.setLeftBorderStyle(null);
        //cell.setBottomBorderStyle(null);
        
        row = table.createRow(50);
        cell = row.createCell(90, "<b>Aspectes del centre</b>");
        cell.setRightBorderStyle(null);
        cell.setLeftBorderStyle(null);
        //cell.setBottomBorderStyle(null);
        cell.setFontSize(12);
        cell.setValign(VerticalAlignment.BOTTOM);
        cell = row.createCell(15, "");
        cell.setLeftBorderStyle(null);
        cell.setRightBorderStyle(null);
        //cell.setBottomBorderStyle(null);
        
        row = table.createRow(40);
        cell = row.createCell(35, "<b>Línea pedagògica del PEC a destacar: </b>");
        cell = row.createCell(70, visita.getLiniaPedagogica());
        
        row = table.createRow(40);
        cell = row.createCell(35, "<b>Projectes i aspectes a destacar: </b>");
        cell = row.createCell(70, visita.getProjectesDestacables());
        
        row = table.createRow(20);
        cell = row.createCell(25, "<b>iMoute: </b>");
        cell = row.createCell(10, visita.getiMoute()==1?"Si":"No");
        cell = row.createCell(25, "<b>Erasmus+:</b> ");
        cell = row.createCell(10, visita.getErasmus()==1?"Si":"No");
        cell = row.createCell(25, "<b>PIEE: </b>");
        cell = row.createCell(10, visita.getPIIE()==1?"Si":"No");
        
        row = table.createRow(40);
        cell = row.createCell(35, "<b>Impacte de la formació en el centre: </b>");
        cell = row.createCell(70, visita.getImpacte());
        
        row = table.createRow(40);
        cell = row.createCell(35, "<b>Participació en accions formatives: </b>");
        cell = row.createCell(70, visita.getParticipacioAF());
        
        row = table.createRow(50);
        cell = row.createCell(90, "<b>Fases del PAF</b>");
        cell.setRightBorderStyle(null);
        cell.setLeftBorderStyle(null);
        cell.setValign(VerticalAlignment.BOTTOM);
        //cell.setBottomBorderStyle(null); 
        cell.setFontSize(12);
        cell = row.createCell(15, "");
        cell.setLeftBorderStyle(null);
        cell.setRightBorderStyle(null);
        //cell.setBottomBorderStyle(null);
        
        row = table.createRow(40);
        cell = row.createCell(35, "<b>FASE I. Primeres impressions: </b>");
        cell = row.createCell(70, visita.getFIPrimeresImpr());
        
        row = table.createRow(40);
        cell = row.createCell(35, "<b>FASE I. Dificultats trobades: </b>");
        cell = row.createCell(70, visita.getFIDificultatsTrobades());
        
        row = table.createRow(40);
        cell = row.createCell(35, "<b>FASE II. Desenvolupament: </b>");
        cell = row.createCell(70, visita.getFIIDesenvolupament());
        
        row = table.createRow(40);
        cell = row.createCell(35, "<b>FASE II. Dificultats trobades: </b>");
        cell = row.createCell(70, visita.getFIIDificultatsTroben());
        
        row = table.createRow(50);
        cell = row.createCell(90, "<b>Altres qüestions d'interés</b>");
        cell.setRightBorderStyle(null);
        cell.setLeftBorderStyle(null);
        cell.setValign(VerticalAlignment.BOTTOM);
        cell = row.createCell(15, "");
        cell.setLeftBorderStyle(null);
        cell.setRightBorderStyle(null);
        cell.setFontSize(12);
        row = table.createRow(50);
        cell = row.createCell(90, visita.getEspais());
        cell.setRightBorderStyle(null);
        cell = row.createCell(15, "");
        cell.setLeftBorderStyle(null);

		  
        table.draw();
        cos.newLine();
     
        cos.close();		
        document.save(outputFileName);
        document.close();       
        
        //System.out.println("He acabat");     

    }
    

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			Agafa_arxiu (outputFileName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}