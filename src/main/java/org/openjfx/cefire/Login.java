/**
 * 
 */
package org.openjfx.cefire;

import java.io.IOException;
import java.net.URL;

/**
 * @author alviboi
 *
 */

import java.sql.SQLException;
import java.util.ResourceBundle;

import data.UsersDAO;
import domain.Users;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

/**
 * Esquelet del controlador de la finestra Login
 * 
 * @author alfredo
 *
 */
public class Login implements Initializable {

    @FXML
    private TextField emailIdField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private Button submitButton;

    @FXML
    public void login(ActionEvent event) throws SQLException {
    	
    	UsersDAO userdao = new UsersDAO();

        Window owner = submitButton.getScene().getWindow();

        if (emailIdField.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, owner, "Error d'entrada!",
                "Introdueix correu electrònic");
            return;
        }
        if (passwordField.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, owner, "Error d'entrada!",
                "Introdueix password");
            return;
        }

        String emailId = emailIdField.getText();
        String password = passwordField.getText();
        
        Users logat = userdao.validateUser(emailId, password);

        if (logat == null) {
        	showAlert(Alert.AlertType.ERROR, owner, "Error d'entrada!",
                    "Les dades no són correctes");
        } else {
        	obri_finestra("primary");
        	Stage thisStage = (Stage) emailIdField.getScene().getWindow();
        	thisStage.close();
        }
    }

    public static void infoBox(String infoMessage, String headerText, String title) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setContentText(infoMessage);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.showAndWait();
    }

    private static void showAlert(Alert.AlertType alertType, Window owner, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.initOwner(owner);
        alert.show();
    }
    
    /**
     * Obri la finestra
     * 
     * @param fxml
     */
    private void obri_finestra (String fxml) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(fxml+".fxml"));
        Parent root1 = null;
		try {
			root1 = (Parent) fxmlLoader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        Scene scene2 = new Scene(root1);
        stage.setTitle("Pantalla principal");
        stage.getIcons().add(new Image("cefireicon.png"));
        stage.setScene(scene2);
        stage.show();
        
    }

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub

		//obri_finestra("primary");
		
	}
}