package org.openjfx.cefire.exporta_xls;

import java.util.List;
import javafx.collections.ObservableList;

/**
 * Aquesta classe agafa totes les dades necessàries per a crear el informe
 * 
 * @author alviboi
 *
 */
public class Curs {
	private String nom;
	private String codi;
	private ObservableList<String> sessions;
	private ObservableList<String> participants;
	
	public Curs(String nom, String codi, ObservableList<String> sessions, ObservableList<String> participants) {
		super();
		this.nom = nom;
		this.codi = codi;
		this.sessions = sessions;
		this.participants = participants;
	}
	public Curs() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the codi
	 */
	public String getCodi() {
		return codi;
	}
	/**
	 * @param codi the codi to set
	 */
	public void setCodi(String codi) {
		this.codi = codi;
	}
	/**
	 * @return the sessions
	 */
	public List<String> getSessions() {
		return sessions;
	}
	/**
	 * @param sessions the sessions to set
	 */
	public void setSessions(ObservableList<String> sessions) {
		this.sessions = sessions;
	}
	/**
	 * @return the participants
	 */
	public List<String> getParticipants() {
		return participants;
	}
	/**
	 * @param participants the participants to set
	 */
	public void setParticipants(ObservableList<String> participants) {
		this.participants = participants;
	}
	
	
	
}
