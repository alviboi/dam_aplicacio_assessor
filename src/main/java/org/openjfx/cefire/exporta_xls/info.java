/**
 * 
 */
package org.openjfx.cefire.exporta_xls;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 * Avisor en popup per al paquet
 * 
 * @author alviboi
 *
 */
public class info {
	
	@SuppressWarnings("exports")
	public static Alert infoBox2(String infoMessage, String headerText, String title, AlertType a) {
        Alert alert = new Alert(a);
        // AlertType.WARNING INFORMATION INFORMATION CONFIRMATION
        alert.setAlertType(a);
        alert.setContentText(infoMessage);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.show();
        return alert;

    }
	
	public static void infoBox(String infoMessage, String headerText, String title, @SuppressWarnings("exports") AlertType a) {
        Alert alert = new Alert(a);
        // AlertType.WARNING INFORMATION INFORMATION CONFIRMATION
        alert.setAlertType(a);
        alert.setContentText(infoMessage);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.showAndWait();

    }
}
