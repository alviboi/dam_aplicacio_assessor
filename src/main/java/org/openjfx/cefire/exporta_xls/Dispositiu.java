package org.openjfx.cefire.exporta_xls;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * En aquesta classe getionem els dispositius que guardem en un fitxer
 * 
 * @author alviboi
 *
 */
public class Dispositiu {
	String ip;
	String nom;
	public Dispositiu(String ip, String nom) {
		super();
		this.ip = ip;
		this.nom = nom;
	}
	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}
	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	/**
	 * Guarda l'arxiu en un fitxer
	 * 
	 * @return
	 * @throws ParseException
	 */
	@SuppressWarnings("unchecked")
	public boolean guarda_dispositiu() throws ParseException {
        JSONObject obj = new JSONObject();
        JSONParser parser = new JSONParser();
        FileWriter file = null;
        JSONArray jsonObject = new JSONArray();
        File f = new File("dispositius.txt"); 
        obj.put("Nom", this.getNom());
        // Per a que siga realment una ip
        Pattern pattern = Pattern.compile("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$");
        Matcher matcher = pattern.matcher(this.getIp());
        if (matcher.matches()) {
        	obj.put("IP", this.getIp());
        } else {
        	return false;        
        }            
		try {
			if (f.exists()) {
				Object llegit_arxiu = parser.parse(new FileReader("dispositius.txt"));
				jsonObject = (JSONArray) llegit_arxiu;			
				System.out.println(jsonObject.toJSONString());
			}
			for (Object object : jsonObject) {
				JSONObject obj_aux = new JSONObject();
				obj_aux = (JSONObject) object;
				System.out.println(obj_aux.get("IP").toString());
				System.out.println(this.getIp());
				if (obj_aux.get("IP").toString().equals(this.getIp())) {
					return false;
				} 
			}
			jsonObject.add(obj);			
            file = new FileWriter("dispositius.txt");
            file.write(jsonObject.toJSONString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
            	if (file != null) {
            		file.flush();
                    file.close();
                    return true;
            	}                
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
		return false;
	}
	
	/**
	 * Borrem un dispositiu
	 * 
	 * @return
	 * @throws ParseException
	 */
	public boolean borra_dispositiu() throws ParseException {
        //JSONObject obj = new JSONObject();
        JSONParser parser = new JSONParser();
        FileWriter file = null;
        JSONArray jsonObject = new JSONArray();
        File f = new File("dispositius.txt");         
		try {
			if (f.exists()) {
				Object llegit_arxiu = parser.parse(new FileReader("dispositius.txt"));
				jsonObject = (JSONArray) llegit_arxiu;			
				System.out.println(jsonObject.toJSONString());
				JSONObject obj_aux2 = new JSONObject();
				for (Object object : jsonObject) {					
					obj_aux2 = (JSONObject) object;
					// Codi per a depurar
					System.out.println("CLASE" + this.getIp());
					System.out.println("AUX" + obj_aux2.get("IP").toString());
					if (obj_aux2.get("IP").toString().equals(this.getIp())) {
						
						jsonObject.remove(object);
						System.out.println("Borre");
						System.out.println(jsonObject.toJSONString());
						System.out.println("Arrive ací");
						file = new FileWriter("dispositius.txt");
			            file.write(jsonObject.toJSONString());
			            break;
					} 
				}				
				
			}	
         
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                file.flush();
                file.close();
                return true;
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
		return false;
	}
	
	
}
