package org.openjfx.cefire.exporta_xls;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import org.json.simple.JSONObject;

/**
 * Aquesta classe gestiona els enviaments al dipositiu que estarà a l'Aula
 * 
 * @author alfredo
 *
 */
public class Envia_socket implements Runnable	{
	Socket cliente;
	String ip;
	JSONObject send;
	JSONObject receive;
	final int Puerto = 12345;
	PrintWriter fsalida;
	BufferedReader fentrada;
	String rebut2 = "";
	Exporta_a_dispositiu exporta_a_dispositiu;
	
	@SuppressWarnings("exports")
	public Envia_socket(Socket ip, JSONObject send, Exporta_a_dispositiu exporta_a_dispositiu) throws UnknownHostException, IOException {
		//this.cliente = new Socket(ip, Puerto);
		this.cliente = ip;
		this.send = send;
		this.exporta_a_dispositiu = exporta_a_dispositiu;
	}



	@Override
	public void run() {
		// TODO Auto-generated method stub
		int intent = 0;
		
		System.out.println(send.toJSONString());
		
			System.out.println(this.getClass());
			try {
				fsalida = new PrintWriter (cliente.getOutputStream(), true);
				fsalida.println(send);
			} catch (IOException e) {
				e.printStackTrace();
			}
	
			try {
				fentrada = new BufferedReader(new InputStreamReader(cliente.getInputStream()));
				System.out.println(fentrada.toString());
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		while (!this.cliente.isClosed()) {	 
			try {
				rebut2=fentrada.readLine();
				System.out.println("Rebut2: " + rebut2);
				if (rebut2 == null) {
					if (intent < 10) {
						intent++;
						exporta_a_dispositiu.avisa_missatge("Intent de comunicació: "+intent);
					} else {
						rebut2 = "No ha hagut resposta del dispositiu";
						exporta_a_dispositiu.avisa_missatge(rebut2);
						cliente.close();
					}				
				} else {
					exporta_a_dispositiu.avisa_missatge(rebut2);
				}
							
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				Thread.sleep(1500);
				if (rebut2 != "") {					
					exporta_a_dispositiu.amaga_missatge();
				} else if (rebut2 == null) {
					cliente.close();
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if (send.get("command") == "Para") {
				try {
					cliente.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		
		};
		System.out.println("Ixc: " + Thread.activeCount());
		
	}

}
