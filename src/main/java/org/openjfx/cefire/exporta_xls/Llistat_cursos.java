/**
 * 
 */
package org.openjfx.cefire.exporta_xls;

import java.net.URL;
import java.util.ResourceBundle;

import data.Ctrl_titulDAO;
import domain.Ctrl_titul;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 * Aquesta classe es el controlador de la finestra del llistat de cursos que hem definit
 * 
 * @author alfredo
 *
 */
public class Llistat_cursos implements Initializable{
	
	private DataSender dataSender;
	
	ObservableList<Ctrl_titul> llistat_cursos_obs;
	
	Ctrl_titulDAO titul_bd;
	
	String codi;
	
    @FXML
    private ProgressBar progress_window;
	
    @FXML
    private Button Tanca_item;

    @FXML
    private Button Edita_item;

    @FXML
    private Button Esborra_item;
    
    @FXML
    private ListView<Ctrl_titul> list_cursos;
    
    @FXML
    void Tanca_finestra(ActionEvent event) {
    	Stage stage = (Stage) Tanca_item.getScene().getWindow();
        stage.close(); 
    }

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		list_cursos.setCellFactory(new Callback<ListView<Ctrl_titul>, ListCell<Ctrl_titul>>() {
		    @Override
		    public ListCell<Ctrl_titul> call(ListView<Ctrl_titul> param) {
		    	ListCell<Ctrl_titul> listcell = new ListCell<Ctrl_titul>()		    	
		    	{
		    		@Override
		            public void updateItem(Ctrl_titul item, boolean empty) {
		                super.updateItem(item, empty);
		                if (empty) {
		                    setText(null);
		                } else {
		                	setFont(Font.font("Verdana", FontWeight.BOLD, 17));
		                    setText(item.getCodi() + ": " + item.getTitul());
		                }
		            }
		        };
		        
		        return listcell;
		    }
		});
		progress_window.setVisible(true);
		progress_window.setProgress(0);
		
		Thread one = new Thread() {
		    public void run() {
		    	titul_bd = new Ctrl_titulDAO();
				llistat_cursos_obs = FXCollections.observableArrayList(titul_bd.listitems(progress_window));
				progress_window.setProgress(1);
				list_cursos.setItems(llistat_cursos_obs);
				progress_window.setVisible(false);			
		    }  
		};
		one.start();		
	}
	
    /**
     * Quan volem editar un curs
     * 
     * @param event
     */
    @FXML
    void Edita_curs(ActionEvent event) {
    	Gestor_bd_sessions gestor = new Gestor_bd_sessions();
    	String codi = list_cursos.getSelectionModel().getSelectedItem().getCodi();
    	Curs send = new Curs();
    	if (codi == null || codi.isEmpty()) {
    		info.infoBox("Has de seleccionar un item", "Error en selecció de item", "Error", AlertType.WARNING);
    	} else {
    		// TODO: POSAR PROGRESS BAR
    		send = gestor.llig_curs(codi, null);
        	dataSender.send_data(send);
    	}
    	Tanca_finestra(null);
    	
    }
    

    /**
     * Quan tenim que editar un curs enviem l'objecte a la finestra principal
     * 
     * @param ds
     */
    public void setSendDataSender(DataSender ds){
        this.dataSender=ds;
    }
    
    /**
     * Borrar un curs
     * 
     * @param event
     */
    @FXML
    void borra_item(ActionEvent event) {
    	Gestor_bd_sessions gestor = new Gestor_bd_sessions();
    	Ctrl_titul item_remove = list_cursos.getSelectionModel().getSelectedItem();    	
    	gestor.borra_curs(item_remove.getCodi());
    	llistat_cursos_obs.remove(item_remove);
    }

}

