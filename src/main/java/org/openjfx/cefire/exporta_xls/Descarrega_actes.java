package org.openjfx.cefire.exporta_xls;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import data.Ctrl_titulDAO;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;

/**
 * Aquesta classe mosgra l'esquelet de la finestra per a descarregar les actes d'assistència
 * 
 * @author alfredo
 *
 */
public class Descarrega_actes implements Initializable {

	List<Object[]> sessions_codi_data;
	
    @FXML
    private Text avis_txt;

    @FXML
    private ProgressIndicator indicador;
	
    @FXML
    private ComboBox<String> combox_curs_sessio;

    @FXML
    private ProgressBar progress_window;

    @FXML
    void descarrega_acta(ActionEvent event) {
    	indicador.setVisible(true);
    	
    	FileChooser fileChooser = new FileChooser();
    	String codigo = combox_curs_sessio.getValue().split(": ")[0].toString();
    	String sessio = combox_curs_sessio.getValue().split(": ")[1].toString();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PDF arxius (*.pdf)", "*.pdf");
        fileChooser.getExtensionFilters().add(extFilter);
        File file = fileChooser.showSaveDialog(combox_curs_sessio.getScene().getWindow());
        
        if (file != null) {
        	new Thread(new Crea_pdf(sessio, codigo, file.getAbsolutePath(), indicador, avis_txt)).start();
			//Crea_pdf.Agafa_arxiu(data_Escollida.getValue().getYear()-1900, data_Escollida.getValue().getMonthValue()-1, file.getAbsolutePath(), progresarxiu);					
        }
    }
    
    @Override
	public void initialize(URL location, ResourceBundle resources) {
		
		progress_window.setVisible(true);
		progress_window.setProgress(0.1);
		indicador.setVisible(false);
		avis_txt.setVisible(false);
		
		Thread th = new Thread() {
		    public void run() {
		        	Gestor_bd_sessions sess_bd = new Gestor_bd_sessions();
		        	progress_window.setProgress(0.2);
		    		sessions_codi_data = sess_bd.agafa_cursos_sessions();
		    		progress_window.setProgress(0.3);
		    		for (Object[] object : sessions_codi_data) {
		    			combox_curs_sessio.getItems().add(object[0].toString()+": "+object[1].toString()+ ": "+ Ctrl_titulDAO.get_titul(object[0].toString()));
		    		}
		    		progress_window.setProgress(0.5);
		    		progress_window.setProgress(1);
		    		progress_window.setVisible(false);
		    		combox_curs_sessio.setDisable(false);		       
		    }  
		};

		th.start();
		
	}

}
