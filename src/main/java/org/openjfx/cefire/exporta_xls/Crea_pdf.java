package org.openjfx.cefire.exporta_xls;

import java.time.LocalDate;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import be.quodlibet.boxable.*;
//import be.quodlibet.boxable.line.LineStyle;
import data.Ctrl_assistentDAO;
import data.Ctrl_codiDAO;
import data.Ctrl_titulDAO;
import domain.Ctrl_assistents;
import domain.Ctrl_codi;
import javafx.scene.text.Text;
import javafx.scene.control.ProgressIndicator;

/**
 * Aquesta classe s'encarrega de crear el pdf del llistat d'assitència.
 * 
 * @author alfredo
 *
 */
public class Crea_pdf implements Runnable{
	
	String sessio;
	String codi; 
	String outputFileName; 
	ProgressIndicator progresarxiu;
	Text arxiu_txt;
	
	
	
	
    public Crea_pdf(String sessio, String codigo, String outputFileName, ProgressIndicator progresarxiu, Text arxiu_txt) {
		super();
		this.sessio = sessio;
		this.codi = codigo;
		this.outputFileName = outputFileName;
		this.progresarxiu = progresarxiu;
		this.arxiu_txt = arxiu_txt;
	}
    

    //ProgressBar Progress_bar
    @SuppressWarnings("unused")
	public static void Agafa_arxiu (String data, String codi, String outputFileName, ProgressIndicator progresarxiu, Text arxiu_txt ) throws Exception {
		
    	LocalDate hui = LocalDate.now();
		
		Ctrl_codiDAO ctrlcodidao = new Ctrl_codiDAO();
		Ctrl_assistentDAO ctrlassistentdao = new Ctrl_assistentDAO();
		Ctrl_titulDAO ctrltituldao = new Ctrl_titulDAO();
		List a = ctrlcodidao.get_asssitents(codi, data);
		
		String titul = ctrltituldao.get_titul(codi);
		
        PDDocument document = new PDDocument();
		int progres = 0;
		
//		for (Object object : a) {
//			
//
//	        progresarxiu.setProgress(progres/a.size());
//			progres++;
//			Ctrl_codi o = (Ctrl_codi) object;
//			System.out.println(o.getDNI());
//			
//			
//		}
		PDPage page = new PDPage(PDRectangle.A4);
        PDRectangle rect = page.getMediaBox();
        document.addPage(page);
        
        PDPageContentStream cos = new PDPageContentStream(document, page);
        
        //Dummy Table
        float margin = 50;
        float yStartNewPage = page.getMediaBox().getHeight() - (2 * margin);
        float tableWidth = page.getMediaBox().getWidth() - (2 * margin);

        boolean drawContent = true;
        float yStart = yStartNewPage;
        float bottomMargin = 70;
        float yPosition = 650;

        BaseTable table = new BaseTable(yPosition, yStartNewPage, bottomMargin, tableWidth, margin, document, page, true, drawContent);
        
        Row<PDPage> row = table.createRow(20);
        
        //Afegir Logo CEFIRE
        PDImageXObject imageXObject = PDImageXObject.createFromFile("/home/alfredo/Escritorio/índice.png", document);

        cos.drawImage(imageXObject, 330, 780, imageXObject.getWidth()/2,imageXObject.getHeight()/2);
        
        
        //Títol de informe
        cos.beginText();

        cos.setFont(PDType1Font.TIMES_ROMAN, 12);
        cos.setLeading(14.5f);

        cos.newLineAtOffset(160, 730);
        String line1 = "INFORME D'ASSISTÈNCIA DEL CEFIRE DE VALÈNCIA";
        cos.showText(line1);
        
        cos.newLine();
        String line3 = "Curs: "+ codi + " " + titul;
        cos.showText(line3);
        
        cos.newLine();
        String line2 = "Sessió: "+ data;
        cos.showText(line2);   

        //cos.newLine();
        
        Cell<PDPage> cell = row.createCell(35, "DNI");
        cell = row.createCell(35, "Nom");
        cell = row.createCell(35, "Assistència");
        row = table.createRow(20);
        
        
		
		for (Object object : a) {
			progres++;
			
			progresarxiu.setProgress(progres/a.size());

			Ctrl_codi o = (Ctrl_codi) object;

			Ctrl_assistents o2 = ctrlassistentdao.get_item(Integer.toString(o.getDNI()));  
			cell = row.createCell(35, o2.getNom());
            cell = row.createCell(35, Integer.toString(o.getDNI()));
            cell = row.createCell(35, o.isAssistix()? "Assistix" : "NO Assistix");

            row = table.createRow(20);
	        

		}
		
		table.draw();

        cos.close();
        
        document.save(outputFileName);

        document.close();
        
        Thread.sleep(2000);
        progresarxiu.setVisible(false);

    }
  
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			Agafa_arxiu (sessio, codi, outputFileName, progresarxiu, arxiu_txt );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}