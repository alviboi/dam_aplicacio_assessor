package org.openjfx.cefire.exporta_xls;

import java.util.List;

import data.Ctrl_assistentDAO;
import data.Ctrl_codiDAO;
import data.Ctrl_titulDAO;
import domain.Ctrl_assistents;
import domain.Ctrl_codi;
import domain.Ctrl_titul;
import java.sql.Timestamp;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ProgressBar;

/**
 * Aquesta classe s'encarrega de gestionar totes les connexions a la base de dades, es fa en una mateixa classe per a simplificar el manteniment
 * 
 * @author alfredo
 *
 */
public class Gestor_bd_sessions {

	Ctrl_assistentDAO ctrl_assistentdao;
	Ctrl_codiDAO ctrl_codidao;
	Ctrl_titulDAO ctrl_tituldao;
	
	public Gestor_bd_sessions() {
		this.ctrl_assistentdao = new Ctrl_assistentDAO();
		this.ctrl_codidao = new Ctrl_codiDAO();
		this.ctrl_tituldao = new Ctrl_titulDAO();
	}
	
	public void borra_curs(String codi) {
		ctrl_codidao.remove(codi);
		ctrl_tituldao.remove(codi);
		
	}
	

	public void afegix_curs(String codi, String titul, ObservableList<String> items_sessio, ObservableList<String> items_participants) {
//		if (ctrl_tituldao.get_titul("1234567890a").equals("Sense Títol")) {
//			borra_curs(codi);
//		}
		// TODO Auto-generated method stub
		Ctrl_titul titul_bd = new Ctrl_titul(codi, titul);
		ctrl_tituldao.save_item(titul_bd);
		
		for (String participant : items_participants) {
			String[] arr = participant.split(" - ");
			System.out.println(arr[0]);
			System.out.println(arr[1]);
			Ctrl_assistents assistent = new Ctrl_assistents(arr[0],Integer.parseInt(arr[1]));
			ctrl_assistentdao.save_item(assistent);
			for (String sessio : items_sessio) {
				Timestamp data_sessio = Timestamp.valueOf(sessio+":00");
				Ctrl_codi ass_codi = new Ctrl_codi(codi, data_sessio, Integer.parseInt(arr[1]), false);
				ctrl_codidao.save_item(ass_codi);
			}		
		}	
	}
	
	public Curs llig_curs(String codi, ProgressBar prog) {
		
		Curs ret = new Curs();
		
		
		ret.setNom(ctrl_tituldao.get_titul(codi));
		ret.setCodi(codi);
		
		List a_session = ctrl_codidao.get_sessions(codi);
		ObservableList<String> arr_sess = FXCollections.observableArrayList();		
		for (Object object : a_session) {
			String sessio = object.toString().substring(0, 16);
			arr_sess.add(sessio);
			
		}
		ret.setSessions(arr_sess);
		
		List a2 = ctrl_codidao.get_asssitents(codi);	
		ObservableList<String> arr_nom = FXCollections.observableArrayList();		
		for (Object object : a2) {
			Ctrl_assistents nom = ctrl_assistentdao.get_item(object.toString());
			arr_nom.add(nom.getNom()+" - 0"+object.toString());
			
		}
		ret.setParticipants(arr_nom);
		return ret;
	}
	
	public List<Object[]> agafa_cursos_sessions() {
		// TODO Auto-generated method stub
		
		List<Object[]> ret = ctrl_codidao.sessions_codi();
		return ret;
		
	}


}
