package org.openjfx.cefire.exporta_xls;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import data.Ctrl_titulDAO;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ProgressBar;
import javafx.scene.text.Text;
import javafx.stage.WindowEvent;

/**
 * Aquesta classe gestiona els enviaments als dispositius per a que arranquen l'aplicació
 * 
 * @author alfredo
 *
 */
public class Exporta_a_dispositiu implements Initializable {
	Socket cliente;

	final int Puerto = 12345;
		
	ArrayList <Socket> clientes = new ArrayList<Socket>();
	
    @FXML
    private Text avisa_missatge;
	
	@FXML
    private ProgressBar progress_window;
	
    @FXML
    private ComboBox<String> combox_curs_sessio;
    
    @FXML
    private ComboBox<String> combo_box_dispositiu;

    List<Object[]> sessions_codi_data;
    JSONArray llistat_dispositius;
    
    /**
     * Envia l'orde de parar al dispositiu del curs
     * 
     * @param event
     */
    @SuppressWarnings("unchecked")
	@FXML
    void Envia_curs_para_a_dispositiu(ActionEvent event) {
    	if (combo_box_dispositiu.getValue() == null) {
    		info.infoBox("Cal que emplenes tots els camps correctament o no puc arrancar el dispositiu", "Error d'entrada", "Error", AlertType.WARNING);
    	} else {
    		String ip = combo_box_dispositiu.getValue().split(": ")[1].toString();
        	for (Socket socket : clientes) {
        		InetAddress remoteIP = socket.getInetAddress();
        		if (remoteIP.toString().equals("/"+ip)) {
        			JSONObject send_para = new JSONObject();
                	send_para.put("command", "Para");
                	try {   
                		new Thread(new Envia_socket(socket ,send_para, this)).start();
                		clientes.remove(socket);
                		break;
            		} catch (IOException e) {
            			// TODO Auto-generated catch block
            			info.infoBox(e.toString(), "Error d'excepció", "Error", AlertType.ERROR);
            		}
        		}		
			}
    	}	
    }
    
    /**
     * Envia l'orde d'arrancar al dispositiu
     * 
     * @param event
     */
    @SuppressWarnings("unchecked")
	@FXML
    void Envia_curs_a_dispositiu(ActionEvent event) {
    	combox_curs_sessio.getScene().getWindow().addEventFilter(WindowEvent.WINDOW_CLOSE_REQUEST, this::closeWindowEvent);
    	if (combo_box_dispositiu.getValue() == null || combox_curs_sessio.getValue() == null ) {
    		info.infoBox("Cal que emplenes tots els camps correctament o no puc arrancar el dispositiu", "Error d'entrada", "Error", AlertType.WARNING);
    	} else {
    		String ip = combo_box_dispositiu.getValue().split(": ")[1].toString();
        	String codigo = combox_curs_sessio.getValue().split(": ")[0].toString();
        	String sessio = combox_curs_sessio.getValue().split(": ")[1].toString();
        	String titul = combox_curs_sessio.getValue().split(": ")[2].toString();
        	System.out.println(ip +"  "+ codigo +"  "+ sessio);
        	
        	JSONObject send = new JSONObject();
        	send.put("command", "Arranca");
        	send.put("codigo", codigo);
        	send.put("sessio", sessio);
        	send.put("titul", titul);
        	System.out.println("Entre, num threads: " + Thread.activeCount());
        	System.out.println(send.toJSONString());
        	if (ip.isEmpty() || codigo.isEmpty() || sessio.isEmpty()) {
        		
        	} else {
        		boolean començat = false;
        		for (Socket socket : clientes) {
            		InetAddress remoteIP = socket.getInetAddress();
            		if (remoteIP.toString().equals("/"+ip)) {
            			info.infoBox("Ja has començat una connexió amb aquesta ip. Alguna cosa estàs fent malament", "Error en la orde", "Error", AlertType.WARNING);
            			començat = true;
            			break;
            		}		
    			}
        		if (!començat) {
        			try {
            			Socket cliente_aux = new Socket(ip, Puerto);
            			clientes.add(cliente_aux);
            			try {
            				new Thread(new Envia_socket(cliente_aux ,send, this)).start();
            			} catch (IOException e) {
            				// TODO Auto-generated catch block
            				info.infoBox(e.toString(), "Error d'excepció", "Error", AlertType.ERROR);
            			}
            		} catch (UnknownHostException e1) {
            			// TODO Auto-generated catch block
            			info.infoBox(e1.toString(), "Error d'excepció", "Error", AlertType.ERROR);
            		} catch (IOException e1) {
            			// TODO Auto-generated catch block
            			info.infoBox(e1.toString(), "Error d'excepció", "Error", AlertType.ERROR);
            		}
        		}		
        	}
    	}
    		
    }
    
    public void avisa_missatge(String str) {
    	avisa_missatge.setVisible(true);
    	avisa_missatge.setText(str);
    }
    
    public void amaga_missatge() {
    	avisa_missatge.setVisible(false);
    }
    

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		avisa_missatge.setVisible(false);
		progress_window.setVisible(true);
		progress_window.setProgress(0.1);
		
		Thread th = new Thread() {
		    public void run() {
		        	Gestor_bd_sessions sess_bd = new Gestor_bd_sessions();
		        	progress_window.setProgress(0.2);
		    		sessions_codi_data = sess_bd.agafa_cursos_sessions();
		    		progress_window.setProgress(0.3);
		    		for (Object[] object : sessions_codi_data) {
		    			combox_curs_sessio.getItems().add(object[0].toString()+": "+object[1].toString()+ ": "+ Ctrl_titulDAO.get_titul(object[0].toString()));
		    		}
		    		progress_window.setProgress(0.5);
		    		Emplena_dispositius();
		    		progress_window.setProgress(1);
		    		progress_window.setVisible(false);
		    		combox_curs_sessio.setDisable(false);
		    		combo_box_dispositiu.setDisable(false);
		       
		    }  
		};

		th.start();
		
	}
	
	/**
	 * Llig tots els dispositius de l'arxiu per a mostrarlos en la taula
	 */
	public void Emplena_dispositius() {
				File f = new File("dispositius.txt");
		        JSONParser parser = new JSONParser();
				try {
					if (f.exists()) {
						Object llegit_arxiu = parser.parse(new FileReader("dispositius.txt"));
						llistat_dispositius = (JSONArray) llegit_arxiu;			
					}			
			
		        } catch (IOException | ParseException e) {
		        	info.infoBox(e.toString(), "Error d'excepció", "Error", AlertType.ERROR);
		        }
				progress_window.setProgress(0.8);
				for (Object object : llistat_dispositius) {
					JSONObject aux = (JSONObject)object;
					combo_box_dispositiu.getItems().add(aux.get("Nom").toString()+": "+aux.get("IP").toString());
				}
	}

	
	private void closeWindowEvent(WindowEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setHeaderText(null);
        alert.setTitle("Compte");
        String str = "";
        for (Socket socket : clientes) {
			str += socket.getInetAddress().toString()+"; ";
		}
        
        alert.setContentText("Aquesta finestra gestiona en estos moments els dispositius "+ str +" Estàs segur de sortir? Ja no pdoràs gestionar els dispositius des d'aquest terminal si tanques la finestra");
        Optional<ButtonType> action = alert.showAndWait();
        if (action.get() == ButtonType.OK) {
        	for (Socket socket : clientes) {
				try {
					socket.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					info.infoBox(e.toString(), "Error d'excepció", "Error", AlertType.ERROR);
				}
			}
        	info.infoBox("S'han tancat totes les connexions als dispositius, cal que tanques el dispositiu des de l'Aula ", "Avís del sistema", "Avís", AlertType.WARNING);
        } else {
        	event.consume();
        }     
    }
}
