package org.openjfx.cefire.exporta_xls;

import org.json.simple.parser.ParseException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;

/**
 * Esquelet de la finestra guarda dispositiu
 * 
 * @author alfredo
 *
 */
public class Dispositiunou {
	
    @FXML
    private TextField ip_txt;

    @FXML
    private TextField nom_txt;

    @FXML
    void Afegix_dispositiu(ActionEvent event) throws ParseException {
    	Dispositiu dispositiu = new Dispositiu(ip_txt.getText(), nom_txt.getText());

    	if(dispositiu.guarda_dispositiu()) {
    		info.infoBox("Guardat correctament", "Dispositiu guardat amb la ip "+ip_txt.getText()+" i nom "+ nom_txt.getText(), "Info", AlertType.CONFIRMATION);
    		ip_txt.setText("");
    		nom_txt.setText("");
    	} else {
    		info.infoBox("Ha hagut un problema en l'arxiu. Són les dades correctes? És possible que el dispositiu ja estiga guardat. No poden haver dos dispositius amb la mateixa IP", "Dispositiu no guardat", "Error", AlertType.ERROR);
    	};
    }

    @FXML
    void comprova_dispositiu(ActionEvent event) {

    }

}
