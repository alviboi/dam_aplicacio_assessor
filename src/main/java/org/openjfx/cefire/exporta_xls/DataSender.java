package org.openjfx.cefire.exporta_xls;

/**
 * Aquesta classe ens permet enviar les dades de si volem editar algun curs
 * 
 * @author alviboi
 *
 */
public interface DataSender {
	
	 void send_data(Curs data);
}
