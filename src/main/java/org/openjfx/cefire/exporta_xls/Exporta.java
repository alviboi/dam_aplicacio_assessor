/**
 * 
 */
package org.openjfx.cefire.exporta_xls;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.openjfx.cefire.App; 

/**
 * Aquesta classe és el controlador de la finestra principal per a exportar les dades al dispositiu
 * 
 * @author alfredo
 *
 */
public class Exporta implements Initializable, DataSender{

	ObservableList<String> items = FXCollections.observableArrayList();

	ObservableList<String> items_sessio = FXCollections.observableArrayList();

	@FXML
	private Text missatge_arrastra;

	@FXML
	private Button Exporta_btn;

	@FXML
	private TextField codi_txt;

	@FXML
	private TextField titul_txt;

	@FXML
	private ListView<String> llistat_extret;

	@FXML
	private ListView<String> llistat_sessions;

	@FXML
	private ComboBox<?> minuts;

	@FXML
	private ComboBox<?> hora;

	@FXML
	private DatePicker data;

	@FXML
	void Activa_curs(ActionEvent event) {
		obri_finestra("exporta_curs_a_dispositiu");
	}

	/**
	 * Aquest mètode que hem de crear, es per aquan volem editar un curs ja creat, per a que pose les dades als camps respectius, l'hem definit en la interface DataSender
	 */
	@Override
	public void send_data(Curs data) {
		// TODO Auto-generated method stub
		//codi_txt.setText(data);
		missatge_arrastra.setText("");

		items.clear();
		items_sessio.clear();
		items = (ObservableList<String>) data.getParticipants();
		items_sessio = (ObservableList<String>) data.getSessions();
		llistat_extret.setItems(items);
		llistat_sessions.setItems(items_sessio);

		titul_txt.setText(data.getNom());
		codi_txt.setText(data.getCodi());
	}

	@FXML
	void Llistat_dispositius(ActionEvent event) {
		obri_finestra("llistat_dispositius");
	}

	@FXML
	void configura_dispositiu(ActionEvent event) {
		obri_finestra("dispositiu_nou");
	}

	@FXML
	void Llistat_cursos(ActionEvent event) {
		obri_finestra("llistat_cursos");
	}

	@FXML
	void Exporta_pdf(ActionEvent event) {
		obri_finestra("descarrega_actes");
	}


	@FXML
	void Obri_buscador_arxius() {
		Stage thisStage = (Stage) llistat_extret.getScene().getWindow();
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Escollix arxiu");
		fileChooser.showOpenDialog(thisStage);
	}    


	/**
	 * 
	 * Per a convertir l'arxiu xls hem de fer una sèrie de modificacions per a que puga ser importat
	 * 
	 * @param event
	 */
	@FXML
	void Importa_xls_odt(DragEvent event) {
		System.out.println("onDragDropped");
		String arxiu = "";
		Dragboard db = event.getDragboard();
		boolean success = false;
		missatge_arrastra.setText("");
		if (db.hasString()) {
			//infoBox(db.getString(), "Alerta", "Alerta");
			String file_db = db.getString();
			System.out.println(file_db.substring(0,5));
			System.out.println(file_db.substring(6));
			System.out.println(file_db.substring(file_db.length()-3,file_db.length()));
			if (db.getString().substring(0,5).equals("file:")) {				
				String extensio = file_db.substring(file_db.length()-3,file_db.length());
				System.out.println("Extensio es: "+extensio);
				if (extensio == "odt") {
					arxiu = db.getString().substring(6);
				} else if (extensio.equals("xls")) {
					String path = Exporta.class.getProtectionDomain().getCodeSource().getLocation().getPath();
					System.out.println(path);
					/* Borrem els arxius si s'han quedat residuals d'abans*/
					File file = new File ("/tmp/convertir.xls");
					File file2 = new File ("/tmp/convertir.ods");
					try {
						boolean result = Files.deleteIfExists(file.toPath());
						boolean result1 = Files.deleteIfExists(file2.toPath());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					var processBuilder = new ProcessBuilder();					
					Path sourceDirectory = Paths.get(db.getString().substring(6));
					Path targetDirectory = Paths.get("/tmp/convertir.xls");

					//System.out.println("Estic ací");

					try {
						Files.copy(sourceDirectory, targetDirectory);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					Runtime r = Runtime.getRuntime();
					String[] commands = {"sh", "-c", "/home/alfredo/Escritorio/converteix.sh"};
					Process process = null;
					try {
						process = r.exec(commands);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					try {
						process.waitFor();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					arxiu = "/tmp/convertir.ods";
				}	

				try {
					items = Excel_llig.llig_excel(arxiu);
					llistat_extret.setItems(items);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println();
			success = true;
		}
		event.setDropCompleted(success);
		event.consume();
	}


	/**
	 * Guarda les dades que hem posat
	 * 
	 * @param event
	 */
	@FXML
	void Exporta(ActionEvent event) {
		if (codi_txt.getText() != null && !items_sessio.isEmpty() && !items.isEmpty()) {
			Gestor_bd_sessions gestor = new Gestor_bd_sessions();
			gestor.afegix_curs(codi_txt.getText(), titul_txt.getText(),items_sessio,items);
			info.infoBox("S'han enviat les dades a la base de dades per a ser guardades", "Enviament correcte", "Avís", AlertType.INFORMATION);

		} else {
			info.infoBox("Has d'omplir tots els camps del formulari, per a poder afegir el curs a la base dedades.", "Error al formulari", "Error", AlertType.WARNING);
		}
	}

	/**
	 * Afegim sessions a la taula de sessions
	 * 
	 * @param event
	 */
	@FXML
	void Afegix_sessio(ActionEvent event) {
		LocalDate data_pick = data.getValue();
		String hora_pick = hora.getValue().toString();
		String minuts_pick = minuts.getValue().toString();

		if (data_pick != null && hora_pick != null && minuts_pick != null) {
			items_sessio.add(data_pick.toString() + " " + hora_pick.toString() + ":" + minuts_pick.toString());
		} else {
			info.infoBox("Revisa la data, l'hora o els minuts", "Error en la data", "Error", AlertType.WARNING);
		}

	}

	@FXML
	void Lleva_sessio(ActionEvent event) {
		items_sessio.remove(llistat_sessions.getSelectionModel().getSelectedIndex());

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		llistat_sessions.setItems(items_sessio);

		llistat_extret.setOnDragOver(event -> {
			event.acceptTransferModes(TransferMode.MOVE);
		});		


	}


	/**
	 * Quan volem obrir una finestra nova ho gestionem tot des d'aquest mètode
	 * 
	 * @param fxml_file
	 */
	public void obri_finestra(String fxml_file) {
		Parent part = null;
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(App.class.getResource("exporta_xls/"+fxml_file+".fxml"));
			part = loader.load();
			//part = FXMLLoader.load(App.class.getResource("exporta_xls/"+fxml_file+".fxml"));		
			if (fxml_file.equals("llistat_cursos")) {
				Llistat_cursos controller = loader.getController(); 
				controller.setSendDataSender(this);
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Stage stage = new Stage();
		if (part != null) {
			Scene scene = new Scene(part);
			stage.setScene(scene);
			stage.setTitle(fxml_file);
			stage.show();
		} else {
			info.infoBox("Ha hagut un error carregant la finestra","Error de programa","Error",AlertType.ERROR);
		}

	}

}
