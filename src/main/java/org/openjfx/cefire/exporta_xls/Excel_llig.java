package org.openjfx.cefire.exporta_xls;

import java.io.File;  
import java.io.IOException;

import com.github.miachm.sods.SpreadSheet;
import com.github.miachm.sods.Sheet;
import com.github.miachm.sods.Range;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


/**
 * Classe que llig extrau els participant del full passat
 * 
 * @author alfredo
 *
 */
public class Excel_llig  
{  
	
	private static ObservableList<String> items = FXCollections.observableArrayList();

	/**
	 * Extrau els participants i els guarda en l'obervable list que s'utilitzarà per a ser mostrat en la taula
	 * 
	 * @param full
	 */
	@SuppressWarnings("exports")
	public static void extrau_participantes (Sheet full) {

		for (int i = 0; i < full.getMaxColumns(); i++) {
			if (full.getRange(0, i).getValue().toString().equals("participante")) {
				Range participantes = full.getRange(1, i, full.getMaxRows()-1);
				
				for (int j = 0; j < participantes.getNumRows(); j++) {
						items.add(participantes.getCell(j, 0).getValue().toString());						
				}
				
			}	
		}
	}
	
	/**
	 * Funció que llig l'arxiu i el passa a la funció anterior
	 * 
	 * @param path
	 * @return
	 * @throws IOException
	 */
	public static ObservableList<String> llig_excel(String path) throws IOException  
	{  

		File file = new File(path);   
		SpreadSheet spreadSheet = new SpreadSheet(file);		
		Sheet full = spreadSheet.getSheet(0);		
		extrau_participantes(full);
		
		return items;
	}  
}  
