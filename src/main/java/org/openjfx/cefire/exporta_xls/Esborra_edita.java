/**
 * És la classe que ens permetrà borrar els dispositius
 */
package org.openjfx.cefire.exporta_xls;

//import java.awt.Insets;
import java.io.File;
import java.io.FileReader;
//import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
//import java.nio.file.Paths;
//import java.util.ArrayList;
import java.util.ResourceBundle;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javafx.collections.FXCollections;
//import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
//import javafx.event.EventHandler;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
//import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 * Aquesta classe és el controlador de la finestra per al llistat de dispositius
 * 
 * @author alfredo
 *
 */
public class Esborra_edita implements Initializable{

	ObservableList <Dispositiu> obs;
	
    @FXML
    private Button Tanca_item;

    @FXML
    private Button Esborra_item;
    
    @FXML
    private ListView<Dispositiu> list_dispositius;
    
    @FXML
    void Tanca_finestra(ActionEvent event) {
    	Stage stage = (Stage) Tanca_item.getScene().getWindow();
        stage.close(); 
    }

	@SuppressWarnings("unchecked")
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		//ArrayList <HBox> llistat_hbox = new ArrayList <HBox>();
		list_dispositius.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		obs = FXCollections.observableArrayList();
		
		
		
/*
 * Deixem el codi açí per si volem treballar més endavant en crear celdes més customitzades
 * */		
		
		
		
//		list_cursos.setCellFactory(new Callback<ListView<Dispositiu>, ListCell<Dispositiu>>() {
//		    @Override
//		    public ListCell<Dispositiu> call(ListView<Dispositiu> param) {
//		    	ListCell listcell = new ListCell<Dispositiu>()		    	
//		    	{
//
//		        	
//		        	//new Image(App.class.getResource("img/book-gf0a809e34_640.png"));
//		            
//		            //private ImageView imageView = new ImageView("creu.png");
//		            private Text textField = new Text("Dispositiu: ");
//		            private Text textField2 = new Text("Adreça: ");
//		            private FontIcon fontIcon = new FontIcon(FontAwesome.LAPTOP);
//		            private FontIcon fontIcon2 = new FontIcon(FontAwesome.SITEMAP);
//		            private Button bt = new Button ("Borra");
//		            private HBox hbox = new HBox(fontIcon,textField, fontIcon2, textField2, bt);
//		            //private BorderPane bp = new BorderPane(textField2, textField, null, null, boto);
//
//		            
//
//		            
//		            @Override
//		            protected void updateItem(Dispositiu item, boolean empty) {
//		            	
//		                if (item == null || empty) {
//		                	setGraphic(null);
//		                } else {
//		                	
//		                	bt.setOnAction(new EventHandler<ActionEvent>() {
//		                        @Override
//		                        public void handle(ActionEvent event) {
//
//		                        	obs.remove(item);
//		                        	list_cursos.getItems().remove(item);                  
//		                        	
//		                            System.out.println("Action: " + item.getNom());
//		                          
//                 
//		                        }
//		                    });
//		                	
//		                	
//		                	
//		                	llistat_hbox.add(hbox);
//		                	
//		                	
//		                	fontIcon.setIconSize(22);
//		                	fontIcon2.setIconSize(22);
//		                	
//		                	fontIcon.setIconColor(Color.GRAY);
//		                	fontIcon2.setIconColor(Color.GRAY);
//		                	hbox.setSpacing(10);
//		                	textField.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
//		                	textField2.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
//		                	textField.setText(item.getIp());
//		                	textField2.setText(item.getNom());
//		                    setGraphic(hbox);
//		                	
//		                }
//		            }
//		           
//		            
//		
//		        };
//		        
//
//		        return listcell;
//		    }
//		});
//		
		
		
		
		
		list_dispositius.setCellFactory(new Callback<ListView<Dispositiu>, ListCell<Dispositiu>>() {
		    @Override
		    public ListCell<Dispositiu> call(ListView<Dispositiu> param) {
		    	ListCell<Dispositiu> listcell = new ListCell<Dispositiu>()		    	
		    	{
		    		@Override
		            public void updateItem(Dispositiu disp, boolean empty) {
		                super.updateItem(disp, empty);
		                if (empty) {
		                    setText(null);
		                } else {
		                	setFont(Font.font("Verdana", FontWeight.BOLD, 20));
		                    setText(disp.getNom() + " " + disp.getIp());
		                }
		            }
		        };
		        
		        return listcell;
		    }
		});
		File f = new File("dispositius.txt");

        JSONParser parser = new JSONParser();
        JSONArray jsonObject = new JSONArray();
		try {
			if (f.exists()) {
				Object llegit_arxiu = parser.parse(new FileReader("dispositius.txt"));
				jsonObject = (JSONArray) llegit_arxiu;			
				//System.out.println(jsonObject.toJSONString());
			}			
	
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

		obs = converteix(jsonObject);
		list_dispositius.setItems(obs);
		
	}
	
    @FXML
    void borra_item(ActionEvent event) throws ParseException {
    	Dispositiu item_remove = list_dispositius.getSelectionModel().getSelectedItem();    	
    	obs.remove(item_remove);
    	item_remove.borra_dispositiu();
    }
    
    ObservableList converteix (JSONArray arr) {
		JSONObject obj = new JSONObject();
		ObservableList obs_r = FXCollections.observableArrayList();
    	for (Object object : arr) {
			obj = (JSONObject) object;
			Dispositiu aux = new Dispositiu(obj.get("IP").toString(), obj.get("Nom").toString());
			obs_r.add(aux);
		}
		return obs_r; 	
    }
	


}

