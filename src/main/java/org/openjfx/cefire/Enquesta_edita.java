/**
 * 
 */
package org.openjfx.cefire;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
/**
 * @author alviboi
 *
 */
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * Controlador de la finestra de enquesta
 * 
 * @author alfredo
 *
 */
public class Enquesta_edita {
	
    @FXML
    private TextArea textmostra;
    @FXML
    private Button tancaboto;

    public TextField txt2;
    
    @FXML
    void tanca_finestra(MouseEvent event) {
    	txt2.setText(textmostra.getText());
    	Stage stage = (Stage) tancaboto.getScene().getWindow();
        stage.close();
    }
    
    void set_text (TextField txt) {
    	this.txt2 = txt;
    	textmostra.setText(txt.getText());

    }
    
    

}

