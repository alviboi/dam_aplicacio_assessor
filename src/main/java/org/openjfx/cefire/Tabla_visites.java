package org.openjfx.cefire;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import data.UsersDAO;
import data.VisitaDAO;
import domain.Centres;
import domain.Users;
import domain.Visita;
import javafx.animation.FadeTransition;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Duration;

/**
 * Aquesta classe mostra l'esquelet de la finestra on es mostren totes les entrevistes
 * 
 * @author alfredo
 *
 */
public class Tabla_visites implements Initializable {
	
    @FXML
    private VBox tot;
	
    @FXML
    private ComboBox<Users> combo_assesor;
	
    @FXML
    private TextField filtre;

    @FXML
    private Button btnClick;

	@FXML
    private TableColumn<Centres,String> colCentre;

    @FXML
    private TableColumn colCodi;

    @FXML
    private TableColumn colData;

    @FXML
    private Button tancaboto;

    @FXML
    private TableView<Visita> tblVisites;
    
    VisitaDAO visitadao;
    
    UsersDAO usersdao;
    
    private ObservableList<Visita> visites;
    
    private ObservableList<Visita> visites_filtrades;
    
    private ObservableList<Users> usuaris;
    
    FadeTransition ft,ft2;

    /**
     * Obri la finestra amb les dades de la visita que volem editar
     * 
     * @param event
     */
    @FXML
    void edita_visita(MouseEvent event) {
    	
			if (tblVisites.getSelectionModel().getSelectedItem() != null) {
		        Visita selectedVisita = tblVisites.getSelectionModel().getSelectedItem();
		        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("enquesta.fxml"));
	            Parent root1 = null;
				try {
					root1 = (Parent) fxmlLoader.load();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		        
		        Enquesta enquesta = (Enquesta) fxmlLoader.getController();
		        enquesta.emplenaCamps(selectedVisita);
	            Stage stage = new Stage();
	            stage.initModality(Modality.APPLICATION_MODAL);
	            stage.setTitle("Visita a "+selectedVisita.getNom());
	            stage.getIcons().add(new Image("cefireicon.png"));
	            Scene scene2 = new Scene(root1);
	            stage.setScene(scene2);

	            stage.show();
		    }
			
    }
    
    /**
     * Filtrem les entrevistes pel nom del centre
     * 
     * @param event
     */
    @FXML
    void filtra_items(KeyEvent event) {
    	FilteredList <Visita> a = null;
    	if (combo_assesor.selectionModelProperty().getValue().getSelectedItem() != null) {
    		FilteredList <Visita> a2 = new FilteredList<Visita>(visites, i -> i.getUser_id() == combo_assesor.selectionModelProperty().getValue().getSelectedItem().getId());
        	a = new FilteredList<Visita>(a2, i -> i.getNom().toLowerCase().contains(filtre.getText().toLowerCase()));
    	} else {
        	a = new FilteredList<Visita>(visites, i -> i.getNom().toLowerCase().contains(filtre.getText().toLowerCase()));

    	}			  	
    	System.out.println(filtre.getText());
    	tblVisites.setItems(a);
    }
      
    /**
     * Filtrem les entrevistes per assessor
     * 
     * @param event
     */
    @FXML
    void filtra_assessor(ActionEvent event) {
    	FilteredList <Visita> a = null;
    	if (!filtre.getText().isEmpty()) {
    		FilteredList <Visita> a2 = new FilteredList<Visita>(visites, i -> i.getNom().toLowerCase().contains(filtre.getText().toLowerCase()));
    		a = new FilteredList<Visita>(a2, i -> i.getUser_id() == combo_assesor.selectionModelProperty().getValue().getSelectedItem().getId());
    	} else {
    		a = new FilteredList<Visita>(visites, i -> i.getUser_id() == combo_assesor.selectionModelProperty().getValue().getSelectedItem().getId());

    	}			  	
    	System.out.println(filtre.getText());
    	tblVisites.setItems(a);
    
    }
    
    /**
     * Aquest mètode elimina tots els centres
     * 
     * @param event
     */
    @FXML
    void filtra_assessor_tots(ActionEvent event) {
    	filtre.setText("");
    	tblVisites.setItems(visites_filtrades);
    	combo_assesor.getSelectionModel().clearSelection();
    	combo_assesor.setValue(null);
    	combo_assesor.setItems(usuaris);
    }

    /**
     * Tanca la finestra
     * 
     * @param event
     */
    @FXML
    void tanca_finestra(ActionEvent event) {
    	Stage stage = (Stage) tblVisites.getScene().getWindow();
        stage.close(); 
    }

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		//visites = FXCollections.observableArrayList();
		ft = new FadeTransition(Duration.millis(500), tblVisites);
        ft.setFromValue(0.2);
        ft.setToValue(0.8);
        ft.setCycleCount(Timeline.INDEFINITE);
        ft.setAutoReverse(true);
        ft.play();
        
        ft2 = new FadeTransition(Duration.millis(500), combo_assesor);
        ft2.setFromValue(0.2);
        ft2.setToValue(0.8);
        ft2.setCycleCount(Timeline.INDEFINITE);
        ft2.setAutoReverse(true);
        ft2.play();
        
        this.colCentre.setCellValueFactory(new PropertyValueFactory("nom"));
		this.colCodi.setCellValueFactory(new PropertyValueFactory("Codi"));
		this.colData.setCellValueFactory(new PropertyValueFactory("Data2"));
		
		
		
		Callback<ListView<Users>, ListCell<Users>> cellFactory = new Callback<ListView<Users>, ListCell<Users>>() {

		    @Override
		    public ListCell<Users> call(ListView<Users> l) {
		        return new ListCell<Users>() {

		            @Override
		            protected void updateItem(Users item, boolean empty) {
		                super.updateItem(item, empty);
		                if (item == null || empty) {
		                    setGraphic(null);
		                } else {
		                    //setText(item.getId() + "    " + item.getName());
		                	setText(item.getName());
		                }
		            }
		        } ;
		    }
		};

		// Just set the button cell here:
		combo_assesor.setButtonCell(cellFactory.call(null));
		combo_assesor.setCellFactory(cellFactory);
		
		Thread one = new Thread() {
		    public void run() {
		    	visitadao = new VisitaDAO();
		    	usersdao = new UsersDAO();
		    	visites = FXCollections.observableArrayList(visitadao.listvisites());
		    	usuaris = FXCollections.observableArrayList(usersdao.listusers());
		    	visites_filtrades = visites;	  
		    	tblVisites.setItems(visites_filtrades);
		    	combo_assesor.setItems(usuaris);
		    	ft.stop();
		    	ft2.stop();
		    	ft.setToValue(1);
		    	ft2.setToValue(1);
		    	ft.setCycleCount(1);
		    	ft2.setCycleCount(1);
		    	ft.play();
		    	ft2.play();
		    	
		    }  
		};

		one.start();
		 
	}
	
	
	
    /**
     * Crea notificació
     * 
     * @param a
     */
    public void alert (String a) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Alert");
        ButtonType type = new ButtonType("Ok", ButtonData.OK_DONE);
        alert.setContentText(a);
        alert.getDialogPane().getButtonTypes().add(type);
        alert.showAndWait();
    }

}