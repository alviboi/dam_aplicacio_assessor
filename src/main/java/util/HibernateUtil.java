// package util.HibernateUtil.java
package util;
import java.util.logging.Level;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
public class HibernateUtil {
	public static final SessionFactory factory = buildSessionFactory();
	private static SessionFactory buildSessionFactory() {
		java.util.logging.Logger.getLogger("org.hibernate").setLevel(Level.OFF);
		try {
			// Create the SessionFactory from hibernate.cfg.xml
			return new Configuration().configure().buildSessionFactory();
		} catch (Throwable ex) {
			// Make sure you log the exception, as it might be swallowed
			System.err.println("SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}
	public static SessionFactory getSessionFactory() {
		return factory;
	}
	public static void shutdown() {
		// Close caches and connection pools
		getSessionFactory().close();
	}
}