module org.openjfx.cefire {
	
    requires javafx.controls;
    requires javafx.fxml;
	requires javafx.graphics;
	requires java.logging;
	//requires java.persistence;
	requires jakarta.persistence;
	requires org.hibernate.orm.core;
	requires java.sql;
	requires javafx.base;
	requires jbcrypt;
	requires java.desktop;
	//requires org.apache.poi.poi;
	//requires org.apache.poi.ooxml;
	//requires com.opencsv;
	requires com.github.miachm.sods;
	requires json.simple;
	//requires org.kordamp.ikonli.fontawesome;
	//requires org.kordamp.ikonli.javafx;
	requires org.apache.pdfbox;
	requires boxable;
	//requires fontawesomefx;

    opens org.openjfx.cefire to javafx.fxml;
    opens org.openjfx.cefire.editor to javafx.fxml;
    opens org.openjfx.cefire.exporta_xls to javafx.fxml;
    opens org.openjfx.cefire.exporta_pdf to javafx.fxml;

    exports org.openjfx.cefire;
    exports org.openjfx.cefire.editor;
    exports org.openjfx.cefire.exporta_xls;
    exports org.openjfx.cefire.exporta_pdf;
    exports data;
    exports domain;
    exports util;
   
}