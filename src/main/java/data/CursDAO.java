package data;
import util.*;
import domain.*;
import java.util.List;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * 
 * Aquesta classe proporciona les dades necessàries de la taula de Curs.
 * 
 * @author alfredo
 *
 */
public class CursDAO {


	public ArrayList <Curs> listCurs() {
		Session session = HibernateUtil.factory.openSession();
		ArrayList <Curs> TotsCurs = new ArrayList <Curs> ();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			@SuppressWarnings("unchecked")
			List Curs = session.createQuery("FROM Curs").list();
			if (Curs.isEmpty())
				System.out.println("******** No items found");
			for (Iterator iterator = Curs.iterator(); iterator.hasNext();) {
				Curs centre = (Curs) iterator.next();
				TotsCurs.add(centre);

			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return TotsCurs;
	}
	
	public ArrayList <Curs> listCurs_mes_user(int any, int mes, int user_id) {
		Date inici = new Date(any, mes, 1);
		Date fi = new Date(any, mes+1, 0);
		
		System.out.println(inici.toString());
		System.out.println(fi.toString());
		Session session = HibernateUtil.factory.openSession();
		ArrayList <Curs> TotsCurs = new ArrayList <Curs> ();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			@SuppressWarnings("unchecked")
			List Curs = session.createQuery("FROM Curs where data > '"+ inici + "' and data < '" + fi +"' and user_id =" + user_id).list();
			if (Curs.isEmpty())
				System.out.println("******** No items found");
			for (Iterator iterator = Curs.iterator(); iterator.hasNext();) {
				Curs centre = (Curs) iterator.next();
				TotsCurs.add(centre);

			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return TotsCurs;
	}
	
	public ArrayList <Curs> listCursusuari(int user_id) {
		Session session = HibernateUtil.factory.openSession();
		ArrayList <Curs> TotsCurs = new ArrayList <Curs> ();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			@SuppressWarnings("unchecked")
			List Curs = session.createQuery("FROM Curs WHERE user_id = :user_id").setParameter("user_id", user_id).list();
			if (Curs.isEmpty())
				System.out.println("******** No items found");
			for (Iterator iterator = Curs.iterator(); iterator.hasNext();) {
				Curs centre = (Curs) iterator.next();
				TotsCurs.add(centre);

			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return TotsCurs;
	}
	
}