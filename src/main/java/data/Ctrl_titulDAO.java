/**
 * 
 */
package data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import domain.Ctrl_titul;
import javafx.scene.control.ProgressBar;
import util.HibernateUtil;

/**
 * 
 * Aquesta classe proporciona les dades necessàries de la taula de Ctrl_titul.
 * 
 * @author alfredo
 *
 */
public class Ctrl_titulDAO {

	public ArrayList <Ctrl_titul> listitems(ProgressBar prog) {
		Session session = HibernateUtil.factory.openSession();
		ArrayList <Ctrl_titul> TotsCtrl_titul = new ArrayList <Ctrl_titul> ();
		Transaction tx = null;
		//int i = 0;
		try {
			tx = session.beginTransaction();
			prog.setProgress(0.3);
			@SuppressWarnings("unchecked")
			List Ctrl_titul = session.createQuery("FROM Ctrl_titul").list();
			if (Ctrl_titul.isEmpty())
				System.out.println("******** No items found");
			prog.setProgress(0.5);
			for (Iterator iterator = Ctrl_titul.iterator(); iterator.hasNext();) {
				//i++;
				Ctrl_titul item = (Ctrl_titul) iterator.next();
				TotsCtrl_titul.add(item);
			}
			prog.setProgress(0.8);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return TotsCtrl_titul;
	}
	
	
	
	public void remove_item (Ctrl_titul item) {
		Session session = HibernateUtil.factory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();			
			session.remove(item);
			Ctrl_codiDAO aux_db = new Ctrl_codiDAO();
			/*
			 * No tenen encara clar si volen mantindre els assistents o no. Per això es deixa en separat i no es borra en cascade en la base de dades.
			 */
			aux_db.remove(item.getCodi());
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
	
	@SuppressWarnings("rawtypes")
	public void remove (String codi) {
		Session session = HibernateUtil.factory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			// https://stackoverflow.com/questions/15913150/what-is-the-proper-way-to-cast-hibernate-
			String deleteQuery = "delete from Ctrl_titul where codi= :codi";
			Query query = session.createQuery(deleteQuery);
			query.setParameter("codi", codi);
			query.executeUpdate();
						
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}

		
	}
	
	
	public void save_item (Ctrl_titul titul) {
		Session session = HibernateUtil.factory.openSession();
		Transaction tx = session.beginTransaction();
		session.merge(titul);
		tx.commit();
		session.close();
		
	}
	
	@SuppressWarnings({ "deprecation", "unchecked" })
	public static String get_titul (String codi) {
		Session session = HibernateUtil.factory.openSession();
		Transaction tx = session.beginTransaction();
		List<Ctrl_titul> Curs = session.createQuery("FROM Ctrl_titul WHERE codi = :codi").setParameter("codi", codi).list();
		
		
		tx.commit();
		session.close();
		if (Curs.isEmpty()) {
			return "Sense Títol";
		} else {
			return Curs.get(0).getTitul();
		}	
	}
}
