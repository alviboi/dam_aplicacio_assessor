package data;
import util.*;
import domain.*;
import java.util.List;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;


/**
 * 
 * Aquesta classe proporciona les dades necessàries de la tabla de CEFIRE.
 * 
 * @author alfredo
 *
 */
public class CefireDAO { 

	
	/**
	 * Per a traure un llistat de totes les entrades de CEFIRE
	 * 
	 * @return el llistat de la tabla
	 */
	public ArrayList <Cefire> listCefire() {
		Session session = HibernateUtil.factory.openSession();
		ArrayList <Cefire> TotsCefire = new ArrayList <Cefire> ();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			// https://stackoverflow.com/questions/15913150/what-is-the-proper-way-to-cast-hibernate-
			
			@SuppressWarnings("unchecked")
			List Cefire = session.createQuery("FROM Cefire").list();
			if (Cefire.isEmpty())
				System.out.println("******** No items found");
			for (Iterator iterator = Cefire.iterator(); iterator.hasNext();) {
				Cefire centre = (Cefire) iterator.next();
				TotsCefire.add(centre);

			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return TotsCefire;
	}
	
	
	/**
	 * Torna el llista de la taula CEFIRE d'un assessor donat un any i un mes
	 * 
	 * @param any
	 * @param mes
	 * @param user_id
	 * @return
	 */
	public ArrayList <Cefire> listCefire_mes_user(int any, int mes, int user_id) {
		//int any = (new Date(System.currentTimeMillis())).getYear();
		@SuppressWarnings("deprecation")
		Date inici = new Date(any, mes, 1);
		@SuppressWarnings("deprecation")
		Date fi = new Date(any, mes+1, 0);
		
		System.out.println(inici.toString());
		System.out.println(fi.toString());
		Session session = HibernateUtil.factory.openSession();
		ArrayList <Cefire> TotsCefire = new ArrayList <Cefire> ();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			// https://stackoverflow.com/questions/15913150/what-is-the-proper-way-to-cast-hibernate-
			
			List Cefire = session.createQuery("FROM Cefire where data > '"+ inici + "' and data < '" + fi +"' and user_id =" + user_id).list();
			if (Cefire.isEmpty())
				System.out.println("******** No items found");
			for (Iterator iterator = Cefire.iterator(); iterator.hasNext();) {
				Cefire centre = (Cefire) iterator.next();
				TotsCefire.add(centre);

			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return TotsCefire;
	}
	
	/**
	 * Torna tot el llistat de CEFIRE de un assessor
	 * 
	 * @param user_id
	 * @return
	 */
	public ArrayList <Cefire> listCefireusuari(int user_id) {
		Session session = HibernateUtil.factory.openSession();
		ArrayList <Cefire> TotsCefire = new ArrayList <Cefire> ();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			// https://stackoverflow.com/questions/15913150/what-is-the-proper-way-to-cast-hibernate-
			
			@SuppressWarnings("unchecked")
			List Cefire = session.createQuery("FROM Cefire WHERE user_id = :user_id").setParameter("user_id", user_id).list();
			if (Cefire.isEmpty())
				System.out.println("******** No items found");
			for (Iterator iterator = Cefire.iterator(); iterator.hasNext();) {
				Cefire centre = (Cefire) iterator.next();
				TotsCefire.add(centre);

			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return TotsCefire;
	}

}