/**
 * 
 */
package data;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.mindrot.jbcrypt.BCrypt;

import domain.Ctrl_assistents;
import domain.Ctrl_codi;
import domain.Ctrl_titul;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import util.HibernateUtil;

/**
 * 
 * Aquesta classe proporciona les dades necessàries de la taula de Ctrl_codi.
 * 
 * @author alfredo
 *
 */
public class Ctrl_codiDAO {

	public ArrayList <Ctrl_codi> listitems() {
		Session session = HibernateUtil.factory.openSession();
		ArrayList <Ctrl_codi> TotsCtrl_codi = new ArrayList <Ctrl_codi> ();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();			
			@SuppressWarnings("unchecked")
			List Ctrl_codi = session.createQuery("FROM Ctrl_codi").list();
			if (Ctrl_codi.isEmpty())
				System.out.println("******** No items found");
			for (Iterator iterator = Ctrl_codi.iterator(); iterator.hasNext();) {
				Ctrl_codi centre = (Ctrl_codi) iterator.next();
				TotsCtrl_codi.add(centre);

			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return TotsCtrl_codi;
	}
	
	public void remove (String codi) {
		Session session = HibernateUtil.factory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			String deleteQuery = "delete from Ctrl_codi where Codi= :codi";
			Query query = session.createQuery(deleteQuery);
			query.setParameter("codi", codi);
			query.executeUpdate();
						
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}

		
	}
	
	public List get_sessions (String codi) {
		Session session = HibernateUtil.factory.openSession();
		Transaction tx = null;
		List sessions = null;

		try {
			tx = session.beginTransaction();
			String sql = "SELECT DISTINCT Data FROM Ctrl_codi WHERE Codi=:codi";
			//Query query = session.createQuery(Query);
			Query query = session.createQuery(sql);
			query.setParameter("codi", codi);			
			sessions = query.getResultList();		
			if (sessions.isEmpty())
				System.out.println("******** No items found");
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return sessions;
	}
	
	public List get_asssitents (String codi) {
		Session session = HibernateUtil.factory.openSession();
		Transaction tx = session.beginTransaction();
		Query query = session.createQuery("SELECT DISTINCT DNI FROM Ctrl_codi WHERE Codi=:codi");
		query.setParameter("codi", codi);			
		List ret = query.getResultList();						
		tx.commit();
		session.close();
		return ret;
		
	}
	
	public List get_asssitents (String codi, String sessio) {
		Session session = HibernateUtil.factory.openSession();
		Transaction tx = session.beginTransaction();
		Query query = session.createQuery("FROM Ctrl_codi WHERE Codi=:codi and Data=:sessio");
		
		Timestamp timestamp = Timestamp.valueOf(sessio);
		
		query.setParameter("codi", codi);	
		query.setParameter("sessio", timestamp);
		List ret = query.getResultList();						
		tx.commit();
		session.close();
		return ret;	
	}
	
	
	public void save_item (Ctrl_codi codi) {
		Session session = HibernateUtil.factory.openSession();
		Transaction tx = session.beginTransaction();
		session.persist(codi);
		tx.commit();
		session.close();
		
	}
	
	public List<Object[]> sessions_codi() {
		Session session = HibernateUtil.factory.openSession();
		Transaction tx = null;
		List<Object[]> sessions = null;

		try {
			tx = session.beginTransaction();
			String sql = "SELECT DISTINCT Codi,Data FROM Ctrl_codi";
			//String sql = "SELECT DISTINCT ctrl_codi.Codi,Data,titul FROM Ctrl_codi LEFT JOIN Ctrl_titul on ctrl_codi.Codi = ctrl_titul.codi";
			//Query query = session.createQuery(Query);
			Query query = session.createQuery(sql);	
			sessions = query.getResultList();						
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return sessions;
	}
	
	public List<Object[]> sessions_codi (String codi) {
		Session session = HibernateUtil.factory.openSession();
		Transaction tx = null;
		List<Object[]> sessions = null;

		try {
			tx = session.beginTransaction();
			String sql = "SELECT DISTINCT Codi,Data FROM nullCtrl_codi where codi=:codi";
			//String sql = "SELECT DISTINCT ctrl_codi.Codi,Data,titul FROM Ctrl_codi LEFT JOIN Ctrl_titul on ctrl_codi.Codi = ctrl_titul.codi";
			//Query query = session.createQuery(Query);
			Query query = session.createQuery(sql);	
			query.setParameter("codi", codi);
			sessions = query.getResultList();
			if (sessions.isEmpty())
				System.out.println("******** No items found");
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return sessions;
	}
}
