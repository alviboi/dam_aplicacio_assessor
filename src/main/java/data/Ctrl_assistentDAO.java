/**
 * 
 */
package data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.mindrot.jbcrypt.BCrypt;

import domain.Ctrl_assistents;
import domain.Ctrl_titul;
import util.HibernateUtil;

/**
 * 
 * Aquesta classe proporciona les dades necessàries de la taula de Ctrl_assistent.
 * 
 * @author alfredo
 *
 */
public class Ctrl_assistentDAO {

	public ArrayList <Ctrl_assistents> listitems() {
		Session session = HibernateUtil.factory.openSession();
		ArrayList <Ctrl_assistents> TotsCtrl_assistents = new ArrayList <Ctrl_assistents> ();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			@SuppressWarnings("unchecked")
			List Ctrl_assistents = session.createQuery("FROM Ctrl_assistents").list();
			if (Ctrl_assistents.isEmpty())
				System.out.println("******** No items found");
			for (Iterator iterator = Ctrl_assistents.iterator(); iterator.hasNext();) {
				Ctrl_assistents centre = (Ctrl_assistents) iterator.next();
				TotsCtrl_assistents.add(centre);

			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return TotsCtrl_assistents;
	}
	
	public void remove (Ctrl_assistents item) {
		Session session = HibernateUtil.factory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			// https://stackoverflow.com/questions/15913150/what-is-the-proper-way-to-cast-hibernate-
			
			session.remove(item);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}

		
	}
	
	public void save_item (Ctrl_assistents ass) {
		Session session = HibernateUtil.factory.openSession();
		Transaction tx = session.beginTransaction();
		session.merge(ass);
		tx.commit();
		session.close();
		
	}
	
	public Ctrl_assistents get_item (String DNI) {
		Session session = HibernateUtil.factory.openSession();
		Transaction tx = session.beginTransaction();
		Ctrl_assistents user =  (Ctrl_assistents) session.get(Ctrl_assistents.class, DNI);
		session.close();
		return user;
		
	}
	
	
}
