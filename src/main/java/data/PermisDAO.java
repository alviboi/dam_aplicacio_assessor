package data;
import util.*;
import domain.*;
import java.util.List;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * 
 * Aquesta classe proporciona les dades necessàries de la taula de Permis.
 * 
 * @author alfredo
 *
 */
public class PermisDAO {


	/* Method to READ all the Permis */
	public ArrayList <Permis> listPermis() {
		Session session = HibernateUtil.factory.openSession();
		ArrayList <Permis> TotsPermis = new ArrayList <Permis> ();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			// https://stackoverflow.com/questions/15913150/what-is-the-proper-way-to-cast-hibernate-
			
			@SuppressWarnings("unchecked")
			List Permis = session.createQuery("FROM Permis").list();
			if (Permis.isEmpty())
				System.out.println("******** No items found");
			for (Iterator iterator = Permis.iterator(); iterator.hasNext();) {
				Permis centre = (Permis) iterator.next();
				TotsPermis.add(centre);

			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return TotsPermis;
	}
	
	public ArrayList <Permis> listPermis_mes_user(int any, int mes, int user_id) {
		//int any = (new Date(System.currentTimeMillis())).getYear();
		Date inici = new Date(any, mes, 1);
		Date fi = new Date(any, mes+1, 0);
		
		System.out.println(inici.toString());
		System.out.println(fi.toString());
		Session session = HibernateUtil.factory.openSession();
		ArrayList <Permis> TotsPermis = new ArrayList <Permis> ();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			// https://stackoverflow.com/questions/15913150/what-is-the-proper-way-to-cast-hibernate-
			
			@SuppressWarnings("unchecked")
			List Permis = session.createQuery("FROM Permis where data > '"+ inici + "' and data < '" + fi +"' and user_id =" + user_id).list();
			if (Permis.isEmpty())
				System.out.println("******** No items found");
			for (Iterator iterator = Permis.iterator(); iterator.hasNext();) {
				Permis centre = (Permis) iterator.next();
				TotsPermis.add(centre);

			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return TotsPermis;
	}
	
	public ArrayList <Permis> listPermisusuari(int user_id) {
		Session session = HibernateUtil.factory.openSession();
		ArrayList <Permis> TotsPermis = new ArrayList <Permis> ();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			// https://stackoverflow.com/questions/15913150/what-is-the-proper-way-to-cast-hibernate-
			
			@SuppressWarnings("unchecked")
			List Permis = session.createQuery("FROM Permis WHERE user_id = :user_id").setParameter("user_id", user_id).list();
			if (Permis.isEmpty())
				System.out.println("******** No items found");
			for (Iterator iterator = Permis.iterator(); iterator.hasNext();) {
				Permis centre = (Permis) iterator.next();
				TotsPermis.add(centre);

			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return TotsPermis;
	}

}