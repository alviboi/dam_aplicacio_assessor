package data;
import util.*;
import domain.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;


/**
 * 
 * Aquesta classe proporciona les dades necessàries de la taula de Centres.
 * 
 * @author alfredo
 *
 */
public class CentresDAO {


	/* Method to READ all the centres */
	public ArrayList <Centres> listcentres() {
		Session session = HibernateUtil.factory.openSession();
		ArrayList <Centres> TotsCentres = new ArrayList <Centres> ();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			// https://stackoverflow.com/questions/15913150/what-is-the-proper-way-to-cast-hibernate-
			
			@SuppressWarnings("unchecked")
			List centres = session.createQuery("FROM Centres").list();
			if (centres.isEmpty())
				System.out.println("******** No items found");
			for (Iterator iterator = centres.iterator(); iterator.hasNext();) {
				Centres centre = (Centres) iterator.next();
				TotsCentres.add(centre);

			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return TotsCentres;
	}
	
	public ArrayList <Centres> listcentresusuari(int user_id) {
		Session session = HibernateUtil.factory.openSession();
		ArrayList <Centres> TotsCentres = new ArrayList <Centres> ();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			// https://stackoverflow.com/questions/15913150/what-is-the-proper-way-to-cast-hibernate-
			
			@SuppressWarnings("unchecked")
			List centres = session.createQuery("FROM Centres WHERE user_id = :user_id").setParameter("user_id", user_id).list();
			if (centres.isEmpty())
				System.out.println("******** No items found");
			for (Iterator iterator = centres.iterator(); iterator.hasNext();) {
				Centres centre = (Centres) iterator.next();
				TotsCentres.add(centre);

			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return TotsCentres;
	}
	
}