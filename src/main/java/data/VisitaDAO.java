package data;
import util.*;
import domain.*;
import jakarta.persistence.criteria.CriteriaQuery;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * 
 * Aquesta classe proporciona les dades necessàries de la taula de Visita.
 * 
 * @author alfredo
 *
 */
public class VisitaDAO {


	/* Method to READ all the Visita */
	public ArrayList <Visita> listvisites() {
		Session session = HibernateUtil.factory.openSession();
		ArrayList <Visita> TotsVisita = new ArrayList <Visita> ();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			// https://stackoverflow.com/questions/15913150/what-is-the-proper-way-to-cast-hibernate-
			
			@SuppressWarnings("unchecked")
			CriteriaQuery cq = session.getCriteriaBuilder().createQuery(Visita.class);
			cq.from(Visita.class);
			List Visita = session.createQuery(cq).getResultList();
			if (Visita.isEmpty())
				System.out.println("******** No items found");
			for (Iterator iterator = Visita.iterator(); iterator.hasNext();) {
				Visita visita = (Visita) iterator.next();
				TotsVisita.add(visita);

			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return TotsVisita;
	}
	

	public Visita get_Visita (int codi, Date data) {
		Session session = HibernateUtil.factory.openSession();
		Transaction tx = null;
		Visita visit = new Visita();
		try {
			tx = session.beginTransaction();
			// https://stackoverflow.com/questions/15913150/what-is-the-proper-way-to-cast-hibernate-
			visit = (Visita) session.createQuery("FROM Visita WHERE codi = :codi and data = :data").setParameter("codi", codi).setParameter("data", data).uniqueResult();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return visit;
		
	}
	
	
	
	public void save_visita (Visita visita) {
		Session session = HibernateUtil.factory.openSession();
		Transaction tx = session.beginTransaction();
		session.merge(visita);
		tx.commit();
		session.close();
	}
	
}