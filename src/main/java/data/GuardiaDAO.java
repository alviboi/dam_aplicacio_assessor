package data;
import util.*;
import domain.*;
import java.util.List;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * 
 * Aquesta classe proporciona les dades necessàries de la taula de Guardia.
 * 
 * @author alfredo
 *
 */
public class GuardiaDAO {


	/* Method to READ all the Guardia */
	public ArrayList <Guardia> listGuardia() {
		Session session = HibernateUtil.factory.openSession();
		ArrayList <Guardia> TotsGuardia = new ArrayList <Guardia> ();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			// https://stackoverflow.com/questions/15913150/what-is-the-proper-way-to-cast-hibernate-
			
			@SuppressWarnings("unchecked")
			List Guardia = session.createQuery("FROM Guardia").list();
			if (Guardia.isEmpty())
				System.out.println("******** No items found");
			for (Iterator iterator = Guardia.iterator(); iterator.hasNext();) {
				Guardia centre = (Guardia) iterator.next();
				TotsGuardia.add(centre);

			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return TotsGuardia;
	}
	
	public ArrayList <Guardia> listGuardia_mes_user(int any, int mes, int user_id) {
		//int any = (new Date(System.currentTimeMillis())).getYear();
		Date inici = new Date(any, mes, 1);
		Date fi = new Date(any, mes+1, 0);
		
		System.out.println(inici.toString());
		System.out.println(fi.toString());
		Session session = HibernateUtil.factory.openSession();
		ArrayList <Guardia> TotsGuardia = new ArrayList <Guardia> ();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			// https://stackoverflow.com/questions/15913150/what-is-the-proper-way-to-cast-hibernate-
			
			@SuppressWarnings("unchecked")
			List Guardia = session.createQuery("FROM Guardia where data > '"+ inici + "' and data < '" + fi +"' and user_id =" + user_id).list();
			if (Guardia.isEmpty())
				System.out.println("******** No items found");
			for (Iterator iterator = Guardia.iterator(); iterator.hasNext();) {
				Guardia centre = (Guardia) iterator.next();
				TotsGuardia.add(centre);

			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return TotsGuardia;
	}
	
	public ArrayList <Guardia> listGuardiausuari(int user_id) {
		Session session = HibernateUtil.factory.openSession();
		ArrayList <Guardia> TotsGuardia = new ArrayList <Guardia> ();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			// https://stackoverflow.com/questions/15913150/what-is-the-proper-way-to-cast-hibernate-
			
			@SuppressWarnings("unchecked")
			List Guardia = session.createQuery("FROM Guardia WHERE user_id = :user_id").setParameter("user_id", user_id).list();
			if (Guardia.isEmpty())
				System.out.println("******** No items found");
			for (Iterator iterator = Guardia.iterator(); iterator.hasNext();) {
				Guardia centre = (Guardia) iterator.next();
				TotsGuardia.add(centre);

			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return TotsGuardia;
	}

}