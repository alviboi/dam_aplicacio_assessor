package data;
import util.*;
import domain.*;
import java.util.List;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * 
 * Aquesta classe proporciona les dades necessàries de la taula de Compensa.
 * 
 * @author alfredo
 *
 */
public class CompensaDAO {


	/* Method to READ all the Compensa */
	public ArrayList <Compensa> listCompensa() {
		Session session = HibernateUtil.factory.openSession();
		ArrayList <Compensa> TotsCompensa = new ArrayList <Compensa> ();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			// https://stackoverflow.com/questions/15913150/what-is-the-proper-way-to-cast-hibernate-
			
			@SuppressWarnings("unchecked")
			List Compensa = session.createQuery("FROM Compensa").list();
			if (Compensa.isEmpty())
				System.out.println("******** No items found");
			for (Iterator iterator = Compensa.iterator(); iterator.hasNext();) {
				Compensa centre = (Compensa) iterator.next();
				TotsCompensa.add(centre);

			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return TotsCompensa;
	}
	
	public ArrayList <Compensa> listCompensa_mes_user(int any, int mes, int user_id) {
		//int any = (new Date(System.currentTimeMillis())).getYear();
		Date inici = new Date(any, mes, 1);
		Date fi = new Date(any, mes+1, 0);
		
		System.out.println(inici.toString());
		System.out.println(fi.toString());
		Session session = HibernateUtil.factory.openSession();
		ArrayList <Compensa> TotsCompensa = new ArrayList <Compensa> ();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			// https://stackoverflow.com/questions/15913150/what-is-the-proper-way-to-cast-hibernate-
			
			@SuppressWarnings("unchecked")
			List Compensa = session.createQuery("FROM Compensa where data > '"+ inici + "' and data < '" + fi +"' and user_id =" + user_id).list();
			if (Compensa.isEmpty())
				System.out.println("******** No items found");
			for (Iterator iterator = Compensa.iterator(); iterator.hasNext();) {
				Compensa centre = (Compensa) iterator.next();
				TotsCompensa.add(centre);

			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return TotsCompensa;
	}
	
	public ArrayList <Compensa> listCompensausuari(int user_id) {
		Session session = HibernateUtil.factory.openSession();
		ArrayList <Compensa> TotsCompensa = new ArrayList <Compensa> ();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			// https://stackoverflow.com/questions/15913150/what-is-the-proper-way-to-cast-hibernate-
			
			@SuppressWarnings("unchecked")
			List Compensa = session.createQuery("FROM Compensa WHERE user_id = :user_id").setParameter("user_id", user_id).list();
			if (Compensa.isEmpty())
				System.out.println("******** No items found");
			for (Iterator iterator = Compensa.iterator(); iterator.hasNext();) {
				Compensa centre = (Compensa) iterator.next();
				TotsCompensa.add(centre);

			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return TotsCompensa;
	}
	
}