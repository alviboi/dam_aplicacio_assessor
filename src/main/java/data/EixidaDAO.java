package data;
import util.*;
import domain.*;
import java.util.List;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * 
 * Aquesta classe proporciona les dades necessàries de la taula de Eixida.
 * 
 * @author alfredo
 *
 */
public class EixidaDAO {


	public ArrayList <Eixides> listEixides() {
		Session session = HibernateUtil.factory.openSession();
		ArrayList <Eixides> TotsEixides = new ArrayList <Eixides> ();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			@SuppressWarnings("unchecked")
			List Eixides = session.createQuery("FROM Eixides").list();
			if (Eixides.isEmpty())
				System.out.println("******** No items found");
			for (Iterator iterator = Eixides.iterator(); iterator.hasNext();) {
				Eixides centre = (Eixides) iterator.next();
				TotsEixides.add(centre);

			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return TotsEixides;
	}
	
	public ArrayList <Eixides> listEixides_mes_user(int any, int mes, int user_id) {
		Date inici = new Date(any, mes, 1);
		Date fi = new Date(any, mes+1, 0);
		
		System.out.println(inici.toString());
		System.out.println(fi.toString());
		Session session = HibernateUtil.factory.openSession();
		ArrayList <Eixides> TotsEixides = new ArrayList <Eixides> ();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();			
			@SuppressWarnings("unchecked")
			List Eixides = session.createQuery("FROM Eixides where data > '"+ inici + "' and data < '" + fi +"' and user_id =" + user_id).list();
			if (Eixides.isEmpty())
				System.out.println("******** No items found");
			for (Iterator iterator = Eixides.iterator(); iterator.hasNext();) {
				Eixides centre = (Eixides) iterator.next();
				TotsEixides.add(centre);

			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return TotsEixides;
	}
	
	public ArrayList <Eixides> listEixidesusuari(int user_id) {
		Session session = HibernateUtil.factory.openSession();
		ArrayList <Eixides> TotsEixides = new ArrayList <Eixides> ();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();			
			@SuppressWarnings("unchecked")
			List Eixides = session.createQuery("FROM Eixides WHERE user_id = :user_id").setParameter("user_id", user_id).list();
			if (Eixides.isEmpty())
				System.out.println("******** No items found");
			for (Iterator iterator = Eixides.iterator(); iterator.hasNext();) {
				Eixides centre = (Eixides) iterator.next();
				TotsEixides.add(centre);

			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return TotsEixides;
	}
	
}