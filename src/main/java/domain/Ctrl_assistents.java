/**
 * 
 */
package domain;

import java.io.Serializable;

/**
 * Paquet domain per a implementar hibernate en la taula de la base de dades de la classe
 * 
 * @author alfredo
 *
 */
public class Ctrl_assistents  implements Serializable {

	private static final long serialVersionUID = 1L;
	private String Nom;
	private int DNI;
	
	
	public Ctrl_assistents(String nom, int dNI) {
		super();
		Nom = nom;
		DNI = dNI;
	}
	
	public Ctrl_assistents() {
		super();
	}
	/**
	 * @return the nom
	 */
	public String getNom() {
		return Nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		Nom = nom;
	}
	/**
	 * @return the dNI
	 */
	public int getDNI() {
		return DNI;
	}
	/**
	 * @param dNI the dNI to set
	 */
	public void setDNI(int dNI) {
		DNI = dNI;
	}
	
	
	
}
