/**
 * 
 */
package domain;

/**
 * Paquet domain per a implementar hibernate en la taula de la base de dades de la classe
 * 
 * @author alfredo
 *
 */
public class Users {
	private int id;
	private String name; 
	private String email; 
	private String password; 
	private int Perfil; 

	public Users(int id, String name, String email, String password, int perfil, int rfid) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.password = password;
		this.Perfil = perfil;

	}
	public Users() {
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the perfil
	 */
	public int getPerfil() {
		return Perfil;
	}
	/**
	 * @param perfil the perfil to set
	 */
	public void setPerfil(int perfil) {
		Perfil = perfil;
	}

	
	
	
}
