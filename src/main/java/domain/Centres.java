package domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Paquet domain per a implementar hibernate en la taula de la base de dades de la classe
 * 
 * @author alfredo
 *
 */
public class Centres implements Serializable{

	private static final long serialVersionUID = 1L;
	private int id; 
	private String nom; 
	private int codi; 
	private String situacio; 
	private int CP; 
	private String ciutat; 
	private int user_id; 
	private String contacte; 
	private String mail_contacte; 
	private int tlf_contacte; 
	private String Observacions; 

	private Set<Visita> visites = new HashSet<Visita>(0);
	


	/**
	 * @return the visites
	 */
	public Set<Visita> getVisites() {
		return visites;
	}
	/**
	 * @param visites the visites to set
	 */
	public void setVisites(Set<Visita> visites) {
		this.visites = visites;
	}
	public Centres() {
	}
	public Centres(int id, String nom, int codi, String situacio, int CP, String ciutat, int user_id, String contacte,
			String mail_contacte, int tlf_contacte, String observacions) {
		super();
		this.id = id;
		this.nom = nom;
		this.codi = codi;
		this.situacio = situacio;
		this.CP = CP;
		this.ciutat = ciutat;
		this.user_id = user_id;
		this.contacte = contacte;
		this.mail_contacte = mail_contacte;
		this.tlf_contacte = tlf_contacte;
		Observacions = observacions;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the codi
	 */
	public int getCodi() {
		return codi;
	}
	/**
	 * @param codi the codi to set
	 */
	public void setCodi(int codi) {
		this.codi = codi;
	}
	/**
	 * @return the situacio
	 */
	public String getSituacio() {
		return situacio;
	}
	/**
	 * @param situacio the situacio to set
	 */
	public void setSituacio(String situacio) {
		this.situacio = situacio;
	}
	/**
	 * @return the cP
	 */
	public int getCP() {
		return CP;
	}
	/**
	 * @param cP the cP to set
	 */
	public void setCP(int cP) {
		CP = cP;
	}
	/**
	 * @return the ciutat
	 */
	public String getCiutat() {
		return ciutat;
	}
	/**
	 * @param ciutat the ciutat to set
	 */
	public void setCiutat(String ciutat) {
		this.ciutat = ciutat;
	}
	/**
	 * @return the user_id
	 */
	public int getUser_id() {
		return user_id;
	}
	/**
	 * @param user_id the user_id to set
	 */
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	/**
	 * @return the contacte
	 */
	public String getContacte() {
		return contacte;
	}
	/**
	 * @param contacte the contacte to set
	 */
	public void setContacte(String contacte) {
		this.contacte = contacte;
	}
	/**
	 * @return the mail_contacte
	 */
	public String getMail_contacte() {
		return mail_contacte;
	}
	/**
	 * @param mail_contacte the mail_contacte to set
	 */
	public void setMail_contacte(String mail_contacte) {
		this.mail_contacte = mail_contacte;
	}
	/**
	 * @return the tlf_contacte
	 */
	public int getTlf_contacte() {
		return tlf_contacte;
	}
	/**
	 * @param tlf_contacte the tlf_contacte to set
	 */
	public void setTlf_contacte(int tlf_contacte) {
		this.tlf_contacte = tlf_contacte;
	}
	/**
	 * @return the observacions
	 */
	public String getObservacions() {
		return Observacions;
	}
	/**
	 * @param observacions the observacions to set
	 */
	public void setObservacions(String observacions) {
		Observacions = observacions;
	}

		
	

}
