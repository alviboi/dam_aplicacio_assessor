/**
 * 
 */
package domain;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Paquet domain per a implementar hibernate en la taula de la base de dades de la classe
 * 
 * @author alfredo
 *
 */
public class Ctrl_codi  implements Serializable {

	private static final long serialVersionUID = 1L;
	private int id;
	private String Codi;
	private Timestamp Data;
	private int DNI;
	private boolean Assistix;
	public Ctrl_codi(int id, String codi, Timestamp data, int dNI, boolean assistix) {
		super();
		this.id = id;
		Codi = codi;
		Data = data;
		DNI = dNI;
		Assistix = assistix;
	}
	
	public Ctrl_codi(String codi, Timestamp data, int dNI, boolean assistix) {
		super();
		Codi = codi;
		Data = data;
		DNI = dNI;
		Assistix = assistix;
	}
	public Ctrl_codi() {
		super();
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the codi
	 */
	public String getCodi() {
		return Codi;
	}
	/**
	 * @param codi the codi to set
	 */
	public void setCodi(String codi) {
		Codi = codi;
	}
	/**
	 * @return the data
	 */
	public Timestamp getData() {
		return Data;
	}
	/**
	 * @param data the data to set
	 */
	public void setData(Timestamp data) {
		Data = data;
	}
	/**
	 * @return the dNI
	 */
	public int getDNI() {
		return DNI;
	}
	/**
	 * @param dNI the dNI to set
	 */
	public void setDNI(int dNI) {
		DNI = dNI;
	}
	/**
	 * @return the assistix
	 */
	public boolean isAssistix() {
		return Assistix;
	}
	/**
	 * @param assistix the assistix to set
	 */
	public void setAssistix(boolean assistix) {
		Assistix = assistix;
	}
	
	
	
	
}
