package domain;


import java.sql.Time;
import java.util.Date;

/**
 * Paquet domain per a implementar hibernate en la taula de la base de dades de la classe
 * 
 * @author alfredo
 *
 */
public class Eixides {

    private Integer id;
    
    private Integer user_id;

    private Date data;

    private Time inici;
    
    private Time fi;
    
    private String centre;

	@SuppressWarnings("exports")
	public Eixides(Integer id, Integer user_id, Date data, Time inici, Time fi, String centre) {
		super();
		this.id = id;
		this.user_id = user_id;
		this.data = data;
		this.inici = inici;
		this.fi = fi;
		this.centre = centre;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUser_id() {
		return user_id;
	}

	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Time getInici() {
		return inici;
	}

	public void setInici(Time inici) {
		this.inici = inici;
	}

	public Time getFi() {
		return fi;
	}

	public void setFi(Time fi) {
		this.fi = fi;
	}

	public Eixides() {
		super();
	}

	public String getCentre() {
		return centre;
	}

	public void setCentre(String centre) {
		this.centre = centre;
	}
    
    
    
}


