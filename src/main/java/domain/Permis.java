package domain;


import java.sql.Time;
import java.util.Date;

/**
 * Paquet domain per a implementar hibernate en la taula de la base de dades de la classe
 * 
 * @author alfredo
 *
 */
public class Permis {

    private Integer id;
    
    private Integer user_id;

    private Date data;

    private Time inici;
    
    private Time fi;
    
    private String motiu;

	public Permis(Integer id, Integer user_id, Date data, Time inici, Time fi, String motiu) {
		super();
		this.id = id;
		this.user_id = user_id;
		this.data = data;
		this.inici = inici;
		this.fi = fi;
		this.motiu = motiu;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUser_id() {
		return user_id;
	}

	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Time getInici() {
		return inici;
	}

	public void setInici(Time inici) {
		this.inici = inici;
	}

	public Time getFi() {
		return fi;
	}

	public void setFi(Time fi) {
		this.fi = fi;
	}

	public Permis() {
		super();
	}

	public String getMotiu() {
		return motiu;
	}

	public void setMotiu(String motiu) {
		this.motiu = motiu;
	}
    
    
    
}


