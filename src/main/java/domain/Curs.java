package domain;


import java.sql.Time;
import java.util.Date;

public class Curs {

    private Integer id;
    
    private Integer user_id;

    private Date data;

    private Time inici;
    
    private Time fi;
    
    private String curs;



	public Curs(Integer id, Integer user_id, Date data, Time inici, Time fi, String curs) {
		super();
		this.id = id;
		this.user_id = user_id;
		this.data = data;
		this.inici = inici;
		this.fi = fi;
		this.curs = curs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUser_id() {
		return user_id;
	}

	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Time getInici() {
		return inici;
	}

	public void setInici(Time inici) {
		this.inici = inici;
	}

	public Time getFi() {
		return fi;
	}

	public void setFi(Time fi) {
		this.fi = fi;
	}

	public Curs() {
		super();
	}

	public String getCurs() {
		return curs;
	}

	public void setCurs(String curs) {
		this.curs = curs;
	}
    
    
    
}


