/**
 * 
 */
package domain;

import java.io.Serializable;

/**
 * Paquet domain per a implementar hibernate en la taula de la base de dades de la classe
 * 
 * @author alfredo
 *
 */
public class Ctrl_titul  implements Serializable{
	private static final long serialVersionUID = 1L;
	private String Codi;
	private String titul;
	public Ctrl_titul(String codi, String titul) {
		super();
		Codi = codi;
		this.titul = titul;
	}
	public Ctrl_titul() {
		super();
	}
	/**
	 * @return the codi
	 */
	public String getCodi() {
		return Codi;
	}
	/**
	 * @param codi the codi to set
	 */
	public void setCodi(String codi) {
		Codi = codi;
	}
	/**
	 * @return the titul
	 */
	public String getTitul() {
		return titul;
	}
	/**
	 * @param titul the titul to set
	 */
	public void setTitul(String titul) {
		this.titul = titul;
	}
	
	
}
